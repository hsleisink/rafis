<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_report_model extends rafis_model {
		private function get_case_threats($case_id) {
			if (($threats = $this->borrow("case/threats")->get_case_threats($case_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($threats as $threat) {
				$result[$threat["id"]] = $threat;
			}

			return $result;
		}

		private function get_controls($case_id) {
			$query = "select distinct m.* from controls m, case_threat_control l, case_threats t, cases c ".
					 "where m.id=l.control_id and l.case_threat_id=t.id and t.case_id=c.id and c.id=%d ".
			         "and m.standard_id=c.standard_id order by id";

			if (($controls = $this->db->execute($query, $case_id)) === false) {
				return false;
			}

			$query = "select l.* from case_threat_control l, case_threats t ".
					 "where l.case_threat_id=t.id and t.case_id=%d and control_id=%d order by t.id";
			foreach ($controls as $m => $control) {
				if (($threats = $this->db->execute($query, $case_id, $control["id"])) === false) {
					return false;
				}

				$controls[$m]["threats"] = array();
				foreach ($threats as $threat) {
					array_push($controls[$m]["threats"], $threat["case_threat_id"]);
				}
			}

			return $controls;
		}

		private function get_systems_for_threat($threat_id, $case_id) {
			$query = "select b.* from bia b, case_threat_bia t, case_scope s ".
					 "where b.id=t.bia_id and t.case_threat_id=%d and s.bia_id=b.id and s.case_id=%d";

			if (($systems = $this->db->execute($query, $threat_id, $case_id)) === false) {
				return false;
			}

			foreach ($systems as $key => $system) {
				$this->decrypt($systems[$key], "item", "description", "impact");

				$a = $system["availability"] - 1;
				$i = $system["integrity"] - 1;
				$c = $system["confidentiality"] - 1;
				$systems[$key]["value"] = $this->asset_value[$i][$c][$a] ?? 0;
			}

			return $systems;
		}

		private function sort_threats($threat_a, $threat_b) {
			if (($threat_a["relevant"] == false) && $threat_b["relevant"]) {
				return 1;
			} else if ($threat_a["relevant"] && ($threat_b["relevant"] == false)) {
				return -1;
			}

			if ($threat_a["risk_value"] < $threat_b["risk_value"]) {
				return 1;
			} else if ($threat_a["risk_value"] > $threat_b["risk_value"]) {
				return -1;
			}

			$systems_a = is_array($threat_a["systems"]) ? count($threat_a["systems"]) : 0;
			$systems_b = is_array($threat_b["systems"]) ? count($threat_b["systems"]) : 0;
			if ($systems_a < $systems_b) {
				return 1;
			} elseif ($systems_a > $systems_b) {
				return -1;
			}

			if (($threat_a["number"] ?? 0) > ($threat_b["number"] ?? 0)) {
				return 1;
			} else if (($threat_a["number"] ?? 0) < ($threat_b["number"] ?? 0)) {
				return -1;
			}

			return 0;
		}

		private function sort_controls($control_a, $control_b) {
			if ((($control_a["risk"] ?? "") == "") && (($control_b["risk"] ?? "") != "")) {
				return 1;
			} else if ((($control_a["risk"] ?? "") != "") && (($control_b["risk"] ?? "") == "")) {
				return -1;
			}

			if (is_false($control_a["relevant"]) && is_true($control_b["relevant"])) {
				return 1;
			} else if (is_true($control_a["relevant"]) && is_false($control_b["relevant"])) {
				return -1;
			}

			if ($control_a["urgency"] < $control_b["urgency"]) {
				return 1;
			} else if ($control_a["urgency"] > $control_b["urgency"]) {
				return -1;
			}

			if (($control_a["asset_value"] ?? 0) < ($control_b["asset_value"] ?? 0)) {
				return 1;
			} else if (($control_a["asset_value"] ?? 0) > ($control_b["asset_value"] ?? 0)) {
				return -1;
			}

			return version_compare($control_a["number"], $control_b["number"]);
		}

		private function valid_url($url) {
			$parts = explode("/", $url, 4);
			if (count($parts) < 4) {
				return false;
			}

			list($protocol,, $hostname, $path) = $parts;

			switch ($protocol) {
				case "http:": $http = new \Banshee\Protocol\HTTP($hostname); break;
				case "https:": $http = new \Banshee\Protocol\HTTPS($hostname); break;
				default: return false;
			}

			if (($result = $http->GET("/".$path)) == false) {
				return false;
			}

			if ($result["status"] != 200) {
				return false;
			}

			return true;
		}

		public function draw_risk_matrix($pdf, $threats, $relevant) {
			$count = 0;

			$matrix = array();
			foreach ($threats as $threat) {
				if (($threat["chance"] == 0) || ($threat["impact"] == 0)) {
					continue;
				} else if ($relevant == ($threat["handle"] == THREAT_ACCEPT)) {
					continue;
				}

				if (is_array($matrix[$threat["chance"] - 1] ?? null) == false) {
					$matrix[$threat["chance"] - 1] = array_fill(0, count($this->risk_matrix_impact), "");
				}

				$matrix[$threat["chance"] - 1][$threat["impact"] - 1]++;
				$count++;
			}

			$pdf->SetFont("helvetica", "", 10);

			$cell_width = 29;
			$cell_height = 8;

			$pdf->Cell($cell_width, $cell_height);
			$pdf->Cell(150, $cell_height, "impact", 0, 0, "C");
			$pdf->Ln();
			$pdf->Cell($cell_width, $cell_height, "kans");
			foreach ($this->risk_matrix_impact as $impact) {
				$pdf->Cell($cell_width, $cell_height, $impact, 1, 0, "C");
			}
			$pdf->Ln();
			$max_y = count($this->risk_matrix_impact) - 1;
			foreach (array_reverse($this->risk_matrix_chance) as $y => $chance) {
				$pdf->Cell($cell_width, $cell_height, $chance, 1);
				foreach ($this->risk_matrix_impact as $x => $impact) {
					$risk = $this->risk_matrix_labels[$this->risk_matrix[$max_y - $y][$x]];
					$pdf->SetColor($risk);
					$pdf->Cell($cell_width, $cell_height, $matrix[$max_y - $y][$x] ?? "", 1, 0, "C", true);
				}
				$pdf->Ln();
			}

			$pdf->Ln(8);

			return $count;
		}

		public function generate_report($case) {
			/* Get threats
			 */
			if (($threats = $this->get_case_threats($case["id"])) === false) {
				return;
			}

			foreach ($threats as $t => $threat) {
				$threats[$t]["risk_value"] = $this->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
				$threats[$t]["relevant"] = ($threat["handle"] != THREAT_ACCEPT) && ($threat["handle"] > 0);
				$threats[$t]["systems"] = $this->get_systems_for_threat($threat["id"], $case["id"]);
			}

			if (is_true($_POST["sort_by_risk"] ?? false)) {
				uasort($threats, array($this, "sort_threats"));
			}

			$nr = 1;
			foreach ($threats as $t => $threat) {
				if ($threats[$t]["relevant"] || is_true($_POST["accepted_risks"] ?? false)) {
					$threats[$t]["number"] = $nr++;
				}
			}

			/* Get controls
			 */
			if (($controls = $this->get_controls($case["id"])) === false) {
				return;
			}

			foreach ($controls as $m => $control) {
				$controls[$m]["urgency"] = 0;
				$controls[$m]["relevant"] = false;

				foreach ($control["threats"] as $threat_id) {
					$threat = $threats[$threat_id];

					if ($threat["risk_value"] > $controls[$m]["urgency"]) {
						$controls[$m]["urgency"] = $threat["risk_value"];
					}

					if ($threat["relevant"]) {
						$controls[$m]["relevant"] = true;
					}
				}
			}

			if (is_true($_POST["sort_by_risk"] ?? false)) {
				usort($controls, array($this, "sort_controls"));
			}

			/* Get BIA items
			 */
			if (($bia_items = $this->borrow("case/scope")->get_scope($case["id"])) === false) {
				return false;
			}

			foreach ($bia_items as $i => $item) {
				$query = "select case_threat_id from case_threat_bia b, case_threats t ".
						 "where t.id=b.case_threat_id and b.bia_id=%d and t.case_id=%d";
				if (($ts = $this->db->execute($query, $item["id"], $case["id"])) === false) {
					return false;
				}

				$bia_items[$i]["threats"] = array();
				foreach ($ts as $t) {
					if (($threat = $threats[$t["case_threat_id"]]) == null) {
						return false;
					}
					if (isset($bia_items[$i]["threats"][$threat["risk_value"]]) == false) {
						$bia_items[$i]["threats"][$threat["risk_value"]] = 1;
					} else {
						$bia_items[$i]["threats"][$threat["risk_value"]]++;
					}
				}
			}

			/* Get scenarios
			 */
			if (($scenarios = $this->borrow("case/scenarios")->get_scenarios($case["id"])) === false) {
				return false;
			}

			$ra_date = date_string("j F Y", strtotime($case["date"]));
			$ra_standard = $this->get_standard($case["standard_id"]);

			/* Generate report
			 */
			$pdf = new RAFIS_report($case["title"]);
			$pdf->SetAuthor($this->get_organisation($this->user->organisation_id)." and RAFIS");
			$pdf->SetSubject("Report risk analysis for information security");
			$pdf->SetKeywords("RAFIS, risk analysis, information security, report");
			$pdf->AliasNbPages();

			/* Title
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Title page");
			if ($case["logo"] != "") {
				if ($this->valid_url($case["logo"])) {
					try {
						$pdf->Image($case["logo"], 15, 20, 0, 25);
					} catch (Exception $e) {
						$pdf->SetFont("helvetica", "", 8);
						$pdf->Cell(0, 0, "[ can't load logo ]");
					}
				} else {
					$pdf->SetFont("helvetica", "", 8);
					$pdf->Cell(0, 0, "[ invalid logo url ]");
				}
			}

			$pdf->SetFont("helvetica", "B", 16);
			$pdf->SetTextColor(54, 94, 145);
			$pdf->Ln(95);
			$pdf->Cell(0, 0, "Report information ", 0, 1, "C");
			$pdf->Ln(7);
			$pdf->Cell(0, 0, "security risk analysis", 0, 1, "C");
			$pdf->SetFont("helvetica", "", 12);
			$pdf->SetTextColor(64, 64, 64);
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $case["title"], 0, 1, "C");
			$pdf->Ln(10);
			$pdf->Cell(0, 0, $ra_date, 0, 1, "C");
			$pdf->Ln(40);
			$pdf->SetFont("helvetica", "I", 12);
			$pdf->Cell(0, 0, "Confidential", 0, 1, "C");

			/* Titel achterblad
			 */
			$pdf->AddPage();
			$pdf->Image("images/layout/rafis_logo.png", 15, 240, 40, 0);
			$pdf->SetFont("helvetica", "", 8);
			$pdf->SetTextColor(128, 128, 128);
			$pdf->Ln(228);
			$pdf->Write(5, "The risk analysis covered by this report was performed using RAFIS. RAFIS is a free and open source method for performing a risk analysis for information security. This method can be found at https://www.rafis.eu/.");

			$pdf->SetFont("helvetica", "", 12);
			$pdf->SetTextColor(0, 0, 0);

			/* Text
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Uitleg");

			$template = file("../extra/report.txt");
			if (is_true($_POST["extra_info"] ?? false)) {
				$template = array_merge($template, file("../extra/report_extra.txt"));
			}

			$text = array();
			$i = 0;
			foreach ($template as $line) {
				switch (substr($line, 0, 1)) {
					case "#":
						break 2;
					case "*":
						$text[++$i] = trim($line);
						break;
					case ">":
					case "-":
						$text[$i++] = trim($line);
						break;
					case "\n":
						$i++;
						break;
					default:
						$line = str_replace("[DATE]", $ra_date, $line);
						$line = str_replace("[ORGANISATION]", $case["organisation"], $line);
						$line = str_replace("[STANDARD]", $ra_standard, $line);
						$text[$i] = (trim($text[$i] ?? "")." ".trim($line));

				}
			}

			$new_page = true;
			$list = false;
			foreach ($text as $i => $line) {
				switch (substr($line, 0, 1)) {
					case ">":
						if ($new_page == false) {
							$pdf->Ln(4);
						}
						if ($list) {
							$pdf->Ln(3);
						}
						$pdf->AddChapter(substr($line, 2));
						$new_page = false;
						$list = false;
						break;
					case "-":
						$pdf->AddPage();
						$new_page = true;
						$list = false;
						break;
					case "*":
						list($head, $line) = explode(":", $line, 2);
						$pdf->Write(5, "» ");
						$pdf->SetFont("helvetica", "I", 10);
						$pdf->Write(5, trim(substr($head, 1)).": ");
						$pdf->SetFont("helvetica", "", 10);
						$pdf->Write(5, $line);
						$pdf->Ln(5);
						$list = true;
						break;
					default:
						if ($list) {
							$pdf->Ln(3);
						}
						$pdf->Write(5, $line);
						$pdf->Ln(8);
						$list = false;
				}
			}

			/* Scenarios
			 */
			if (count($scenarios) > 0) {
				$pdf->AddPage();
				$pdf->Bookmark("Scenarios");
				$pdf->AddChapter("Scenarios");

				$pdf->Write(5, "This chapter contains possible scenarios, based on the concrete threats from the next chapter.");
				$pdf->Ln(8);

				foreach ($scenarios as $scenario) {
					$pdf->SetFont("helvetica", "B", 11);
					$pdf->Write(5, $scenario["title"]);
					$pdf->Ln(5);
					$pdf->SetFont("helvetica", "", 10);
					$pdf->Write(5, $scenario["scenario"]);

					if ($scenario["consequences"] != "") {
						$pdf->Ln(8);
						$pdf->SetFont("helvetica", "I", 10);
						$pdf->Write(5, "Mogelijke gevolgen");
						$pdf->Ln(5);
						$pdf->SetFont("helvetica", "", 10);
						$pdf->Write(5, $scenario["consequences"]);
					}

					$pdf->Ln(10);
				}
			}

			/* Risk matrix
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Risk matrices");
			$pdf->AddChapter("Threat risk matrix");
			$pdf->Write(5, "The table below does not include threats for which the residual risk is accepted.");
			$pdf->Ln(8);
			$this->draw_risk_matrix($pdf, $threats, true);

			/* Control matrix
			 */
			$pdf->AddChapter("Distribution urgency of controls from ".$ra_standard);
			$pdf->Write(5, "The table below does not include controls against threats for which the residual risk is accepted.");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			$matrix = array();
			foreach ($controls as $control) {
				$highest_risk = -1;

				foreach ($threats as $threat) {
					if (in_array($threat["id"], $control["threats"]) == false) {
						continue;
					}
					if (($threat["chance"] == 0) || ($threat["impact"] == 0) || ($threat["handle"] == 0)) {
						continue;
					}
					if ($threat["relevant"] == false) {
						continue;
					}
					$risk = $this->risk_matrix[$threat["chance"] - 1][$threat["impact"] - 1];
					if ($risk >= $highest_risk) {
						$highest_risk = $risk;
					}
				}

				$matrix[$highest_risk] = ($matrix[$highest_risk] ?? 0) + 1;
			}

			foreach ($this->risk_matrix_labels as $i => $level) {
				$pdf->Cell(30, 8, $level, 1, 0, "C");
			}
			$pdf->Ln(8);
			foreach ($this->risk_matrix_labels as $i => $level) {
				$pdf->SetColor($this->risk_matrix_labels[$i]);
				$pdf->Cell(30, 8, sprintf("%d", $matrix[$i] ?? ""), 1, 0, "C", true);
			}
			$pdf->Ln(15);

			/* Accepted risk matrix
			 */
			$pdf->AddChapter("Risk matrices of the accepted threats");
			$pdf->Write(5, "The table below contains an overview of the threats for which the residual risk is accepted.");
			$pdf->Ln(8);
			$accepted_risks = $this->draw_risk_matrix($pdf, $threats, false);

			/* Impact values
			 */
			$impact_values = json_decode($case["impact"], true);
			if ($impact_values[0] != "") {
				$pdf->AddChapter("Definition of the impact");
				$pdf->Write(5, "The impact has the following interpretation for this risk analysis:");
				$pdf->Ln(8);

				foreach ($this->risk_matrix_impact as $i => $impact) {
					$pdf->SetFont("helvetica", "B", 10);
					$pdf->Cell(27, 5, $impact.": ");
					$pdf->SetFont("helvetica", "", 10);
					$pdf->Write(5, $impact_values[$i]);
					$pdf->Ln(5);
				}
			}

			/* Information systems
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Scope");
			$pdf->AddChapter("Scope");
			$scope = rtrim($case["scope"], ".").".";
			$pdf->Write(5, "The risk analysis performed was limited to ".$scope);
			$pdf->Ln(10);

			$this->borrow("case/interests")->add_bia($pdf, $bia_items);

			if ($case["interests"] != "") {
				$pdf->Ln(10);
				$pdf->AddChapter("Interests");
				$pdf->Write(5, $case["interests"]);
			}

			if ($pdf->GetY() > 50) {
				$pdf->AddPage();
			} else {
				$pdf->Ln(10);
			}
			$pdf->Write(5, "The overview below shows the number and level of threats per information system.");
			$pdf->Ln(10);

			$pdf->Cell(75, 6, "", 0, 0);
			foreach ($this->risk_matrix_labels as $label) {
				$pdf->Cell(26, 6, $label, 0, 0, "C");
			}
			$pdf->Ln(6);

			foreach ($bia_items as $item) {
				if ($item["scope"] == NO) {
					continue;
				}

				$pdf->Cell(75, 6, $item["item"], 0, 0);
				foreach ($this->risk_matrix_labels as $i => $label) {
					$pdf->SetColor($label);
					$risk = $item["threats"][$i] ?? "";
					$pdf->Cell(26, 6, $risk, 1, 0, "C", true);
				}
				$pdf->Ln(6);
			}

			$pdf->Ln(15);

			/* Actors
			 */
			$pdf->AddChapter("Actors");
			$pdf->Write(5, "The actors that can possibly breach security.");
			$pdf->Ln(7);

			$this->borrow("case/interests")->add_actors($pdf, $case["id"]);

			/* Threats
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Threats");
			$pdf->AddChapter("Threats");
			$pdf->Write(5, "This chapter provides an overview of all threats and information provided during the threat analysis session.");
			if (is_false($_POST["accepted_risks"] ?? false) && ($accepted_risks > 0)) {
				$pdf->Write(5, "Threats whose residual risk is accepted are omitted in this chapter.");
			}
			$pdf->Ln(12);

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(7, 6, "", "B");
			$pdf->Cell(153, 6, "Threats", "B");
			$pdf->Cell(0, 6, "Risk", "B");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			foreach ($threats as $threat) {
				$risk = $this->risk_matrix_labels[$threat["risk_value"]];

				if ($threat["relevant"] == false) {
					if (is_false($_POST["accepted_risks"] ?? false)) {
						continue;
					} else {
						$risk = "(".$risk.")";
					}
				}

				$pdf->Cell(7, 5, $threat["number"].":");
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->MultiCell(165, 5, $threat["threat"]);
				$pdf->SetFont("helvetica", "", 10);

				$chance = $this->risk_matrix_chance[$threat["chance"] - 1];
				$impact = $this->risk_matrix_impact[$threat["impact"] - 1];

				if ($threat["actor_id"] != null) {
					$actor = $this->borrow("actors")->get_actor($threat["actor_id"]);

					$pdf->Cell(7, 5);
					$pdf->Write(5, "Most threatening actor: ".$actor["name"]);
					$pdf->Ln(5);
				}

				$pdf->Cell(7, 5);
				$pdf->Cell(40, 5, "Chance: ".$chance);
				$pdf->Cell(40, 5, "Impact: ".$impact);
				$pdf->Cell(68, 5, "Approach: ".$this->threat_handle_labels[$threat["handle"] - 1]);

				if ($threat["relevant"]) {
					$pdf->SetColor($risk);
				} else {
					$pdf->SetColor();
				}
				$pdf->Cell(0, 5, $risk, 0, 0, "C", true);
				$pdf->Ln(5);

				if ($threat["action"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, "Desired situation / desired controls:");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $threat["action"]);
				}

				if ($threat["current"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, "Current situation / current controls:");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $threat["current"]);
				}

				if ($threat["argumentation"] != "") {
					$pdf->Cell(7, 5);
					$pdf->MultiCell(150, 5, "Argumentation for the choice made:");
					$pdf->Cell(15, 5);
					$pdf->MultiCell(142, 5, $threat["argumentation"]);
				}

				if ($threat["systems"] != false) {
					$list = array();
					foreach ($threat["systems"] as $system) {
						array_push($list, $system["item"]);
					}

					$pdf->Cell(7, 5);
					$pdf->MultiCell(163, 5, "Involved systems: ".implode(", ", $list));
				}

				$pdf->Ln(1);

				$nr++;
			}

			/* Controls
			 */
			$pdf->AddPage();
			$pdf->Bookmark("Maatregelen");

			$reduce = config_array(CONTROL_REDUCES);

			$pdf->AddChapter("Controls from ".$ra_standard);
			$pdf->Write(5, "This chapter contains an overview of the controls from the ".$ra_standard." standard that are selected based on the specified threats. Use the accompanying texts from this standard to come to a concrete action plan.");
			$pdf->Ln(12);

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(15, 6, "", "B");
			$pdf->Cell(143, 6, "Control", "B");
			$pdf->Cell(0, 6, "Urgency", "B");
			$pdf->Ln(8);

			$pdf->SetFont("helvetica", "", 10);

			foreach ($controls as $control) {
				if (is_false($control["relevant"] ?? false) && is_false($_POST["accepted_risks"] ?? false)) {
					continue;
				}

				$pdf->Cell(15, 5, $control["number"]);
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(140, 5, $control["name"]);
				$pdf->SetFont("helvetica", "", 10);

				$urgency = $this->risk_matrix_labels[$control["urgency"]];
				if ($control["relevant"]) {
					$pdf->SetColor($urgency);
				} else {
					$pdf->SetColor();
					$urgency = "(".$urgency.")";
				}
				$pdf->Cell(0, 5, $urgency, 0, 0, "C", true);
				$pdf->Ln(5);

				$pdf->Cell(15, 5, "");
				$pdf->Cell(100, 5, "This control reduces the ".$reduce[$control["reduce"]]." of an incident.");
				$pdf->Ln(5);

				$pdf->SetColor();
				foreach ($control["threats"] as $threat_id) {
					$threat = $threats[$threat_id];

					if (is_false($_POST["accepted_risks"] ?? false)) {
						if ($threat["relevant"] == false) {
							continue;
						}
						if (($threat["chance"] == 0) || ($threat["impact"] == 0) || ($threat["handle"] == 0)) {
							continue;
						}
					}

					$pdf->Cell(15, 5, "");
					$risk = $this->risk_matrix_labels[$threat["risk_value"]];
					$handle = $this->threat_handle_labels[$threat["handle"] - 1];

					$line = sprintf("%s: %s [ %s, %s ]", $threat["number"], $threat["threat"], $risk, $handle);
					$pdf->MultiCell(150, 5, $line);
				}
				$pdf->Ln(2);
			}

			return $pdf;
		}
	}
?>
