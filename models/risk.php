<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class risk_model extends Banshee\model {
		public function valid_access_code($access_code) {
			$query = "select * from risk_assess_sessions where access_code=%d";

			return $this->db->execute($query, $access_code) != false;
		}

		public function get_session($access_code) {
			$query = "select * from risk_assess_sessions where access_code=%d";
			if (($sessions = $this->db->execute($query, $access_code)) == false) {
				return false;
			}

			return $sessions[0]["id"];
		}

		public function save_vote($vote, $session_id) {
			if (isset($_SESSION["risk_assess_last_insert_id"])) {
				$query = "delete from risk_assess_values where id=%d";

				if ($this->db->execute($query, $_SESSION["risk_assess_last_insert_id"]) === false) {
					return false;
				}
			}

			if (($vote["chance"] < 1) || ($vote["chance"] > 5)) {
				return false;
			}

			if (($vote["impact"] < 1) || ($vote["impact"] > 5)) {
				return false;
			}

			$data = array(
				"id"                     => null,
				"risk_assess_session_id" => $session_id,
				"chance"                 => $vote["chance"],
				"impact"                 => $vote["impact"]);

			if ($this->db->insert("risk_assess_values", $data) === false) {
				return false;
			}

			$_SESSION["risk_assess_last_insert_id"] = $this->db->last_insert_id;

			return true;
		}
	}
?>
