<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_control_model extends Banshee\model {
		public function get_standard($standard) {
			return $this->borrow("cms/standard")->get_item($standard);
		}

		public function count_controls($standard) {
			$query = "select count(*) as count from controls where standard_id=%d";

			if (($result = $this->db->execute($query, $standard)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_controls($standard, $offset, $limit) {
			$query = "select m.*, (select count(*) from mitigation where control_id=m.id) as links from controls m ".
			         "where standard_id=%d order by id limit %d,%d";

			return $this->db->execute($query, $standard, $offset, $limit);
		}

		public function get_control($control_id) {
			if (($result = $this->db->entry("controls", $control_id)) === false) {
				return false;
			}

			$query = "select * from mitigation where control_id=%d";
			if (($links = $this->db->execute($query, $control_id)) === false) {
				return false;
			}

			$result["threat_links"] = array();
			foreach ($links as $link) {
				array_push($result["threat_links"], $link["threat_id"]);
			}
			
			return $result;
		}

		public function get_categories($standard) {
			$query = "select * from control_categories where standard_id=%d order by number";

			if (($categories = $this->db->execute($query, $standard)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["number"]] = $category;
			}

			return $result;
		}

		public function get_threats() {
			$query = "select * from threats order by number";

			return $this->db->execute($query);
		}

		public function save_oke($control) {
			$result = true;

			if ((trim($control["number"]) == "") || (trim($control["name"]) == "")) {
				$this->view->add_message("Please, fill in the number and name.");
				$result = false;
			}

			return $result;
		}

		private function save_threat_links($threat, $control_id) {
			if (is_array($threat["threat_links"] ?? null) == false) {
				return true;
			}

			foreach ($threat["threat_links"] as $threat_id) {
				$data = array(
					"control_id" => $control_id,
					"threat_id"      => $threat_id);
				if ($this->db->insert("mitigation", $data) === false) {
					return false;
				}
			}

			return true;
		}

		public function create_control($control, $standard) {
			$keys = array("id", "standard_id", "number", "name", "reduce");

			$control["id"] = null;
			$control["standard_id"] = $standard;

			if ($this->db->query("begin") === false) {	
				return false;
			}

			if ($this->db->insert("controls", $control, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}
			$control_id = $this->db->last_insert_id;

			if ($this->save_threat_links($control, $control_id) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function update_control($control, $standard) {
			$keys = array("number", "name", "reduce");

			if ($this->db->query("begin") === false) {	
				return false;
			}

			if ($this->db->update("controls", $control["id"], $control, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			$query = "delete from mitigation where control_id=%d";
			if ($this->db->query($query, $control["id"], $standard) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->save_threat_links($control, $control["id"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($control) {
			return true;
		}

		public function delete_control($control_id) {
			$queries = array(
				array("delete from mitigation where control_id=%d", $control_id),
				array("delete from overruled where control_id=%d", $control_id),
				array("delete from controls where id=%d", $control_id));

			return $this->db->transaction($queries);
		}
	}
?>
