<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_threat_model extends Banshee\model {
		public function get_standard($standard) {
			return $this->borrow("cms/standard")->get_item($standard);
		}

		public function count_threats() {
			$query = "select count(*) as count from threats order by number";

			if (($result = $this->db->execute($query)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_threats($standard, $offset, $limit) {
			$query = "select t.*,(select count(*) from mitigation g, controls m ".
			         "where g.control_id=m.id and standard_id=%d and threat_id=t.id) as links from threats t ".
			         "order by number limit %d,%d";

			return $this->db->execute($query, $standard, $offset, $limit);
		}

		public function get_threat($threat_id) {
			if (($result = $this->db->entry("threats", $threat_id)) === false) {
				return false;
			}

			$query = "select * from mitigation where threat_id=%d";
			if (($links = $this->db->execute($query, $threat_id)) === false) {
				return false;
			}

			$result["links"] = array();
			foreach ($links as $link) {
				array_push($result["links"], $link["control_id"]);
			}

			return $result;
		}

		public function get_categories() {
			$query = "select * from threat_categories order by name";

			if (($categories = $this->db->execute($query)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["id"]] = $category;
			}

			return $result;
		}

		public function get_controls($standard) {
			$query = "select * from controls where standard_id=%d";

			return $this->db->execute($query, $standard);
		}

		public function save_oke($threat) {
			$result = true;

			if (valid_input($threat["number"], VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				$this->view->add_message("Number is not a number.");
				$result = false;
			}

			if (trim($threat["threat"]) == "") {
				$this->view->add_message("Fill in the threat.");
				$result = false;
			}

			if (trim($threat["description"]) == "") {
				$this->view->add_message("Fill in the description.");
				$result = false;
			}

			return $result;
		}

		private function save_links($threat, $threat_id) {
			if (is_array($threat["links"] ?? false) == false) {
				return true;
			}

			foreach ($threat["links"] as $control_id) {
				$data = array(
					"control_id" => $control_id,
					"threat_id"      => $threat_id);
				if ($this->db->insert("mitigation", $data) === false) {
					return false;
				}
			}

			return true;
		}

		private function order_numbers($threat_id, $number) {
			$query = "update threats set number=number+1 where id!=%d and number>=%d";
			if ($this->db->query($query, $threat_id, $number) === false) {
				return false;
			}

			$query = "select id,number from threats order by number";
			if (($values = $this->db->execute($query)) === false) {
				return false;
			}

			for ($i = 0; $i < count($values); $i++) {
				$number = $i + 1;
				if ($values[$i]["number"] != $number) {
					if ($this->db->update("threats", $values[$i]["id"], array("number" => $number)) === false) {
						return false;
					}
				}
			}

			return true;
		}

		public function create_threat($threat) {
			$keys = array("id", "number", "threat", "description", "category_id", "confidentiality", "integrity", "availability");

			$threat["id"] = null;

			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->db->insert("threats", $threat, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}
			$threat_id = $this->db->last_insert_id;

			if ($this->save_links($threat, $threat_id) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->order_numbers($threat_id, $threat["number"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function update_threat($threat, $standard) {
			$keys = array("number", "threat", "description", "category_id", "confidentiality", "integrity", "availability");

			if ($this->db->query("begin") === false) {
				return false;
			}

			if ($this->db->update("threats", $threat["id"], $threat, $keys) === false) {
				$this->db->query("rollback");
				return false;
			}

			$query = "delete from mitigation where threat_id=%d and control_id in ".
			         "(select id from controls where standard_id=%d)";
			if ($this->db->query($query, $threat["id"], $standard) === false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->save_links($threat, $threat["id"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			if ($this->order_numbers($threat["id"], $threat["number"]) == false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}

		public function delete_oke($threat) {
			return true;
		}

		public function delete_threat($threat_id) {
			$queries = array(
				array("delete from case_bia_threat where threat_id=%d", $threat_id),
				array("delete from mitigation where threat_id=%d", $threat_id),
				array("delete from case_threat where threat_id=%d", $threat_id),
				array("delete from threats where id=%d", $threat_id));

			return $this->db->transaction($queries);
		}
	}
?>
