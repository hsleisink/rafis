<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_progress_report_controller extends rafis_controller {
		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			if (($report = $this->model->generate_report($this->case)) === false) {
				$this->view->add_tag("result", "Error while generating report.");
			} else {
				$this->view->disable();
				$case_name = $this->generate_filename($this->case["organisation"]." - ".$this->case["name"]);
				$report->Output($case_name." - progress.pdf", "I");
			}
		}
	}
?>
