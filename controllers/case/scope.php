<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class case_scope_controller extends rafis_controller {
		protected $prevent_repost = false;

		private function show_scope($case_id) {
			if (($items = $this->model->get_scope($case_id)) === false) {
				return false;
			}

			$this->view->add_javascript("case/scope.js");

			$this->view->open_tag("overview");
			foreach ($items as $item) {
				$item["value"] = $this->model->asset_value_labels[$item["value"]];
				$item["availability"] = $this->model->availability_score[$item["availability"] - 1] ?? "";
				$item["confidentiality"] = $this->model->confidentiality_score[$item["confidentiality"] - 1] ?? "";
				$item["integrity"] = $this->model->integrity_score[$item["integrity"] - 1] ?? "";
				$item["owner"] = is_true($item["owner"] ?? false) ? "yes" : "no";
				$item["personal_data"] = is_true($item["personal_data"] ?? false) ? "yes" : "no";
				$item["scope"] = is_true($item["scope"] ?? false) ? "yes" : "no";

				$this->view->record($item, "item");
			}
			$this->view->close_tag();
		}

		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$this->model->save_scope($_POST["scope"] ?? null, $case_id);
			}

			$this->show_breadcrumbs($case_id);
			$this->show_scope($case_id);
		}
	}
?>
