<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class riskmatrix_controller extends Banshee\controller {
		public function execute() {
			$this->view->title = "Risk matrix";
			$this->view->description = "De risk matrix as being used within RAFIS.";
			$this->view->keywords = "risk matrix";

			$risk_matrix = config_array(RISK_MATRIX);
			$risk_matrix_labels = config_array(RISK_MATRIX_LABELS);
			$risk_matrix_chance = config_array(RISK_MATRIX_CHANCE);
			$risk_matrix_impact = config_array(RISK_MATRIX_IMPACT);

			$this->view->open_tag("matrix");

			$this->view->open_tag("row");
			$this->view->add_tag("cell", "");
			foreach ($risk_matrix_impact as $impact) {
				$this->view->add_tag("cell", $impact);
			}
			$this->view->close_tag();

			$chance = 4;
			foreach (array_reverse($risk_matrix) as $row) {
				$row = explode(",", $row);

				$this->view->open_tag("row");
				$this->view->add_tag("cell", $risk_matrix_chance[$chance--]);
				foreach ($row as $cell) {
					$this->view->add_tag("cell", $risk_matrix_labels[$cell], array("class" => "risk_".$cell));
				}
				$this->view->close_tag();
			}
			$this->view->close_tag();
		}
	}
?>
