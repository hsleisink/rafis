<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class cms_threat_export_controller extends Banshee\controller {
		public function execute() {
			if (($categories = $this->model->get_categories()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($threats = $this->model->get_threats($_SESSION["standard"])) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$category_id = 0;

			$csv = new \banshee\CSVfile();

			foreach ($threats as $threat) {
				if ($threat["category_id"] != $category_id) {
					$category_id = $threat["category_id"];
					$csv->add_line($categories[$category_id]["name"]);
				}

				if (($controls = $this->model->get_mitigation($threat["id"])) === false) {
					$controls = array();
				}

				$csv->add_line($threat["number"], $threat["threat"], $threat["description"], $controls);
			}

			$csv->to_output($this->view);
		}
	}
?>
