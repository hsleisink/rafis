<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAFIS license.
	 */

	class actors_controller extends rafis_controller {
		private function show_overview() {
			if (($actors = $this->model->get_actors()) === false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$actor_chance = config_array(ACTOR_CHANCE);
			$actor_knowledge = config_array(ACTOR_KNOWLEDGE);
			$actor_resources = config_array(ACTOR_RESOURCES);
			$actor_threat = config_array(ACTOR_THREAT_LABELS);

			$this->view->open_tag("overview");

			$this->view->open_tag("actors");
			foreach ($actors as $actor) {
				$threat_level = $this->model->actor_threat($actor);
				$actor["chance"] = $actor_chance[$actor["chance"] - 1];
				$actor["knowledge"] = $actor_knowledge[$actor["knowledge"] - 1];
				$actor["resources"] = $actor_resources[$actor["resources"] - 1];
				$actor["threat_level"] = $threat_level;
				$actor["threat"] = $actor_threat[$threat_level];
				$this->view->record($actor, "actor");
			}
			$this->view->close_tag();

			$this->view->close_tag();
		}

		private function show_actor_form($actor) {
			$this->view->open_tag("edit");

			$pulldowns = array(
				"chance"    => ACTOR_CHANCE,
				"knowledge" => ACTOR_KNOWLEDGE,
				"resources" => ACTOR_RESOURCES);

			foreach ($pulldowns as $name => $options) {
				$options = config_array($options);
				$this->view->open_tag($name);
				foreach ($options as $option) {
					$this->view->add_tag("item", $option);
				}
				$this->view->close_tag();
			}

			$this->view->record($actor, "actor");

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->add_help_button();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Save actor") {
					/* Save actor
					 */
					if ($this->model->save_oke($_POST) == false) {
						$this->show_actor_form($_POST);
					} else if (isset($_POST["id"]) === false) {
						/* Create actor
						 */
						if ($this->model->create_actor($_POST) === false) {
							$this->view->add_message("Error while creating actor.");
							$this->show_actor_form($_POST);
						} else {
							$this->user->log_action("actor created");
							$this->show_overview();
						}
					} else {
						/* Update actor
						 */
						if ($this->model->update_actor($_POST) === false) {
							$this->view->add_message("Error while updating actor.");
							$this->show_actor_form($_POST);
						} else {
							$this->user->log_action("actor updated");
							$this->show_overview();
						}
					}
				} else if ($_POST["submit_button"] == "Delete actor") {
					/* Delete actor
					 */
					if ($this->model->delete_actor($_POST["id"]) === false) {
						$this->view->add_message("Error while deleting actor.");
						$this->show_actor_form($_POST);
					} else {
						$this->user->log_action("actor deleted");
						$this->show_overview();
					}
				} else {
					$this->show_overview();
				}
			} else if ($this->page->parameter_value(0, "new")) {
				/* New actor
				 */
				$actor = array();
				$this->show_actor_form($actor);
			} else if ($this->page->parameter_numeric(0)) {
				/* Edit actor
				 */
				if (($actor = $this->model->get_actor($this->page->parameters[0])) === false) {
					$this->view->add_tag("result", "Actor not found.\n");
				} else {
					$this->show_actor_form($actor);
				}
			} else {
				/* Show overview
				 */
				$this->show_overview();
			}
		}
	}
?>
