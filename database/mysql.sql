-- MySQL dump 10.19  Distrib 10.3.34-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: rafis_tool
-- ------------------------------------------------------
-- Server version	10.3.34-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actors`
--

DROP TABLE IF EXISTS `actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `chance` tinyint(3) unsigned NOT NULL,
  `knowledge` tinyint(3) unsigned NOT NULL,
  `resources` tinyint(3) unsigned NOT NULL,
  `reason` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`organisation_id`),
  CONSTRAINT `actors_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `advisors`
--

DROP TABLE IF EXISTS `advisors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advisors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `private_key` text NOT NULL,
  `public_key` text NOT NULL,
  `crypto_key` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `organisation_id` (`organisation_id`),
  KEY `advisors_ibfk_2` (`user_id`),
  CONSTRAINT `advisors_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `advisors_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bia`
--

DROP TABLE IF EXISTS `bia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `item` text NOT NULL,
  `description` text NOT NULL,
  `impact` text NOT NULL,
  `availability` tinyint(3) unsigned NOT NULL,
  `integrity` tinyint(3) unsigned NOT NULL,
  `confidentiality` tinyint(3) unsigned NOT NULL,
  `owner` tinyint(3) unsigned NOT NULL,
  `personal_data` tinyint(1) NOT NULL,
  `location` enum('internal','external','saas') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`organisation_id`),
  CONSTRAINT `bia_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cache`
--

DROP TABLE IF EXISTS `cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cache` (
  `key` varchar(100) NOT NULL,
  `value` mediumtext NOT NULL,
  `timeout` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_progress`
--

DROP TABLE IF EXISTS `case_progress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_progress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `executor_id` int(10) unsigned DEFAULT NULL,
  `reviewer_id` int(10) unsigned DEFAULT NULL,
  `control_id` int(10) unsigned DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `info` text NOT NULL,
  `done` tinyint(1) NOT NULL,
  `hours_planned` smallint(5) unsigned NOT NULL,
  `hours_invested` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  KEY `progress_people_id` (`executor_id`),
  KEY `iso_measure_id` (`control_id`),
  KEY `reviewer_id` (`reviewer_id`),
  CONSTRAINT `case_progress_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `case_progress_ibfk_2` FOREIGN KEY (`executor_id`) REFERENCES `users` (`id`),
  CONSTRAINT `case_progress_ibfk_3` FOREIGN KEY (`reviewer_id`) REFERENCES `users` (`id`),
  CONSTRAINT `case_progress_ibfk_4` FOREIGN KEY (`control_id`) REFERENCES `controls` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_scenarios`
--

DROP TABLE IF EXISTS `case_scenarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_scenarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `title` text NOT NULL,
  `scenario` text NOT NULL,
  `consequences` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  CONSTRAINT `case_scenarios_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_scope`
--

DROP TABLE IF EXISTS `case_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_scope` (
  `case_id` int(10) unsigned NOT NULL,
  `bia_id` int(10) unsigned NOT NULL,
  KEY `case_id` (`case_id`),
  KEY `bia_id` (`bia_id`),
  CONSTRAINT `case_scope_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `case_scope_ibfk_2` FOREIGN KEY (`bia_id`) REFERENCES `bia` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_threat_bia`
--

DROP TABLE IF EXISTS `case_threat_bia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_threat_bia` (
  `case_threat_id` int(10) unsigned NOT NULL,
  `bia_id` int(10) unsigned NOT NULL,
  KEY `case_threat_id` (`case_threat_id`),
  KEY `case_bia_id` (`bia_id`),
  CONSTRAINT `case_threat_bia_ibfk_1` FOREIGN KEY (`case_threat_id`) REFERENCES `case_threats` (`id`),
  CONSTRAINT `case_threat_bia_ibfk_2` FOREIGN KEY (`bia_id`) REFERENCES `bia` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_threat_control`
--

DROP TABLE IF EXISTS `case_threat_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_threat_control` (
  `case_threat_id` int(10) unsigned NOT NULL,
  `control_id` int(10) unsigned NOT NULL,
  KEY `case_threat_id` (`case_threat_id`),
  KEY `measure_id` (`control_id`),
  CONSTRAINT `case_threat_control_ibfk_1` FOREIGN KEY (`case_threat_id`) REFERENCES `case_threats` (`id`),
  CONSTRAINT `case_threat_control_ibfk_2` FOREIGN KEY (`control_id`) REFERENCES `controls` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_threats`
--

DROP TABLE IF EXISTS `case_threats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_threats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `threat` text NOT NULL,
  `actor_id` int(10) unsigned DEFAULT NULL,
  `chance` tinyint(4) NOT NULL,
  `impact` tinyint(4) NOT NULL,
  `handle` tinyint(4) NOT NULL,
  `action` text NOT NULL,
  `current` text NOT NULL,
  `argumentation` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `case_id` (`case_id`),
  KEY `actor_id` (`actor_id`),
  CONSTRAINT `case_threats_ibfk_1` FOREIGN KEY (`case_id`) REFERENCES `cases` (`id`),
  CONSTRAINT `case_threats_ibfk_2` FOREIGN KEY (`actor_id`) REFERENCES `actors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `standard_id` int(10) unsigned NOT NULL,
  `name` text NOT NULL,
  `organisation` text NOT NULL,
  `date` date NOT NULL,
  `scope` text NOT NULL,
  `impact` text NOT NULL,
  `interests` text NOT NULL,
  `logo` varchar(250) DEFAULT NULL,
  `archived` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`organisation_id`),
  KEY `iso_standard_id` (`standard_id`),
  CONSTRAINT `cases_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`),
  CONSTRAINT `cases_ibfk_2` FOREIGN KEY (`standard_id`) REFERENCES `standards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `control_categories`
--

DROP TABLE IF EXISTS `control_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `control_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `standard_id` int(10) unsigned NOT NULL,
  `number` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_standard` (`standard_id`),
  CONSTRAINT `control_categories_ibfk_1` FOREIGN KEY (`standard_id`) REFERENCES `standards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `control_categories`
--
-- ORDER BY:  `id`

LOCK TABLES `control_categories` WRITE;
/*!40000 ALTER TABLE `control_categories` DISABLE KEYS */;
INSERT INTO `control_categories` VALUES (1,1,5,'Information security policies'),(2,1,6,'Organization of information security'),(3,1,7,'Human resource security'),(4,1,8,'Asset management'),(5,1,9,'Access control'),(6,1,10,'Cryptography'),(7,1,11,'Physical and environmental security'),(8,1,12,'Operations security'),(9,1,13,'Communications security'),(10,1,14,'System acquisition, development and maintenance'),(11,1,15,'Supplier relationships'),(12,1,16,'Information security incident management'),(13,1,17,'Information security aspects of business continuity management'),(14,1,18,'Compliance'),(15,2,5,'Organizational controls'),(16,2,6,'People controls'),(17,2,7,'Physical controls'),(18,2,8,'Technological controls');
/*!40000 ALTER TABLE `control_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controls`
--

DROP TABLE IF EXISTS `controls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `standard_id` int(10) unsigned NOT NULL,
  `number` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `reduce` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_standard` (`standard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controls`
--
-- ORDER BY:  `id`

LOCK TABLES `controls` WRITE;
/*!40000 ALTER TABLE `controls` DISABLE KEYS */;
INSERT INTO `controls` VALUES (1,1,'5.1.1','Policies for information security',2),(2,1,'5.1.2','Review of the policies for information security',2),(3,1,'6.1.1','Information security roles and responsibilities',2),(4,1,'6.1.2','Segregation of duties',2),(5,1,'6.1.3','Contact with authorities',2),(6,1,'6.1.4','Contact with special interest groups',2),(7,1,'6.1.5','Information security in project management',2),(8,1,'6.2.1','Mobile device policy',2),(9,1,'6.2.2','Teleworking',2),(10,1,'7.1.1','Screening',0),(11,1,'7.1.2','Terms and conditions of employment',2),(12,1,'7.2.1','Management responsibilities',2),(13,1,'7.2.2','Information security awareness, education and training',2),(14,1,'7.2.3','Disciplinary process',0),(15,1,'7.3.1','Termination or change of employment responsibilities',0),(16,1,'8.1.1','Inventory of assets',2),(17,1,'8.1.2','Ownership of assets',2),(18,1,'8.1.3','Acceptable use of assets',0),(19,1,'8.1.4','Return of assets',0),(20,1,'8.2.1','Classification of information',0),(21,1,'8.2.2','Labelling of information',0),(22,1,'8.2.3','Handling of assets',0),(23,1,'8.3.1','Management of removable media',0),(24,1,'8.3.2','Disposal of media',0),(25,1,'8.3.3','Physical media transfer',2),(26,1,'9.1.1','Access control policy',2),(27,1,'9.1.2','Access to networks and network services',2),(28,1,'9.2.1','User registration and de-registration',0),(29,1,'9.2.2','User access provisioning',0),(30,1,'9.2.3','Management of privileged access rights',0),(31,1,'9.2.4','Management of secret authentication information of users',0),(32,1,'9.2.5','Review of user access rights',0),(33,1,'9.2.6','Removal or adjustment of access rights',0),(34,1,'9.3.1','Use of secret authentication information',0),(35,1,'9.4.1','Information access restriction',2),(36,1,'9.4.2','Secure log-on procedures',2),(37,1,'9.4.3','Password management system',0),(38,1,'9.4.4','Use of privileged utility programs',2),(39,1,'9.4.5','Access control to program source code',2),(40,1,'10.1.1','Policy on the use of cryptographic controls',2),(41,1,'10.1.2','Key management',2),(42,1,'11.1.1','Physical security perimeter',2),(43,1,'11.1.2','Physical entry controls',2),(44,1,'11.1.3','Securing offices, rooms and facilities',2),(45,1,'11.1.4','Protecting against external and environmental threats',1),(46,1,'11.1.5','Working in secure areas',0),(47,1,'11.1.6','Delivery and loading areas',0),(48,1,'11.2.1','Equipment siting and protection',0),(49,1,'11.2.2','Supporting utilities',0),(50,1,'11.2.3','Cabling security',0),(51,1,'11.2.4','Equipment maintenance',0),(52,1,'11.2.5','Removal of assets',0),(53,1,'11.2.6','Security of equipment and assets off-premises',2),(54,1,'11.2.7','Secure disposal or re-use of equipment',0),(55,1,'11.2.8','Unattended user equipment',0),(56,1,'11.2.9','Clear desk and clear screen policy',0),(57,1,'12.1.1','Documented operating procedures',2),(58,1,'12.1.2','Change management',0),(59,1,'12.1.3','Capacity management',0),(60,1,'12.1.4','Separation of development, testing and operational environments',0),(61,1,'12.2.1','Controls against malware',0),(62,1,'12.3.1','Information backup',1),(63,1,'12.4.1','Event logging',2),(64,1,'12.4.2','Protection of log information',2),(65,1,'12.4.3','Administrator and operator logs',0),(66,1,'12.4.4','Clock synchronisation',0),(67,1,'12.5.1','Installation of software on operational systems',2),(68,1,'12.6.1','Management of technical vulnerabilities',2),(69,1,'12.6.2','Restrictions on software installation',2),(70,1,'12.7.1','Information systems audit controls',2),(71,1,'13.1.1','Network controls',2),(72,1,'13.1.2','Security of network services',2),(73,1,'13.1.3','Segregation in networks',0),(74,1,'13.2.1','Information transfer policies and procedures',2),(75,1,'13.2.2','Agreements on information transfer',2),(76,1,'13.2.3','Electronic messaging',2),(77,1,'13.2.4','Confidentiality or non-disclosure agreements',2),(78,1,'14.1.1','Information security requirements analysis and specification',2),(79,1,'14.1.2','Securing application services on public networks',2),(80,1,'14.1.3','Protecting application services transactions',2),(81,1,'14.2.1','Secure development policy',2),(82,1,'14.2.2','System change control procedures',2),(83,1,'14.2.3','Technical review of applications after operating platform changes',0),(84,1,'14.2.4','Restrictions on changes to software packages',0),(85,1,'14.2.5','Secure system engineering principles',2),(86,1,'14.2.6','Secure development environment',0),(87,1,'14.2.7','Outsourced development',2),(88,1,'14.2.8','System security testing',2),(89,1,'14.2.9','System acceptance testing',2),(90,1,'14.3.1','Protection of test data',0),(91,1,'15.1.1','Information security policy for supplier relationships',2),(92,1,'15.1.2','Addressing security within supplier agreements',2),(93,1,'15.1.3','Information and communication technology supply chain',2),(94,1,'15.2.1','Monitoring and review of supplier services',2),(95,1,'15.2.2','Managing changes to supplier services',2),(96,1,'16.1.1','Responsibilities and procedures',2),(97,1,'16.1.2','Reporting information security events',2),(98,1,'16.1.3','Reporting information security weaknesses',0),(99,1,'16.1.4','Assessment of and decision on information security events',1),(100,1,'16.1.5','Response to information security incidents',1),(101,1,'16.1.6','Learning from information security incidents',2),(102,1,'16.1.7','Collection of evidence',1),(103,1,'17.1.1','Planning information security continuity',1),(104,1,'17.1.2','Implementing information security continuity',1),(105,1,'17.1.3','Verify, review and evaluate information security continuity',1),(106,1,'17.2.1','Availability of information processing facilities',1),(107,1,'18.1.1',' Identification of applicable legislation and contractual requirements',2),(108,1,'18.1.2','Intellectual property rights',2),(109,1,'18.1.3','Protection of records',2),(110,1,'18.1.4','Privacy and protection of personally identifiable information',2),(111,1,'18.1.5','Regulation of cryptographic controls',2),(112,1,'18.2.1','Independent review of information security',2),(113,1,'18.2.2','Compliance with security policies and standards',2),(114,1,'18.2.3','Technical compliance review',2),(115,2,'5.1','Policies for information security',2),(116,2,'5.2','Information security roles and responsibilities',2),(117,2,'5.3','Segregation of duties',0),(118,2,'5.4','Management responsibilities',0),(119,2,'5.5','Contact with authorities',2),(120,2,'5.6','Contact with special interest groups',2),(121,2,'5.7','Threat intelligence',2),(122,2,'5.8','Information security in project management',2),(123,2,'5.9','Inventory of information and other associated assets',2),(124,2,'5.10','Acceptable use of information and other associated assets',0),(125,2,'5.11','Return of assets',0),(126,2,'5.12','Classification of information',0),(127,2,'5.13','Labelling of information',0),(128,2,'5.14','Information transfer',0),(129,2,'5.15','Access control',2),(130,2,'5.16','Identity management',0),(131,2,'5.17','Authentication information',0),(132,2,'5.18','Access rights',0),(133,2,'5.19','Information security in supplier relationships',2),(134,2,'5.20','Addressing information security within supplier agreements',2),(135,2,'5.21','Managing information security in the ICT supply chain',2),(136,2,'5.22','Monitoring, review and change management of supplier services',2),(137,2,'5.23','Information security for use of cloud services',0),(138,2,'5.24','Information security incident management planning and preparation',2),(139,2,'5.25','Assessment and decision on information security events',0),(140,2,'5.26','Response to information security incidents',1),(141,2,'5.27','Learning from information security incidents',2),(142,2,'5.28','Collection of evidence',1),(143,2,'5.29','Information security during disruption',1),(144,2,'5.30','ICT readiness for business continuity',0),(145,2,'5.31','Legal, statutory, regulatory and contractual requirements',2),(146,2,'5.32','Intellectual property rights',2),(147,2,'5.33','Protection of records',2),(148,2,'5.34','Privacy and protection of PII',2),(149,2,'5.35','Independent review of information security',2),(150,2,'5.36','Conformance with policies, rules and standards for information security',2),(151,2,'5.37','Documented operating procedures',2),(152,2,'6.1','Screening',0),(153,2,'6.2','Terms and conditions of employment',2),(154,2,'6.3','Information security awareness, education and training',2),(155,2,'6.4','Disciplinary process',0),(156,2,'6.5','Responsibilities after termination or change of employment',0),(157,2,'6.6','Confidentiality or non-disclosure agreements',2),(158,2,'6.7','Remote working',2),(159,2,'6.8','Information security event reporting',2),(160,2,'7.1','Physical security perimeters',2),(161,2,'7.2','Physical entry',2),(162,2,'7.3','Securing offices, rooms and facilities',2),(163,2,'7.4','Physical security monitoring',0),(164,2,'7.5','Protecting against physical and environmental threats',1),(165,2,'7.6','Working in secure areas',0),(166,2,'7.7','Clear desk and clear screen',0),(167,2,'7.8','Equipment siting and protection',0),(168,2,'7.9','Security of assets off-premises',2),(169,2,'7.10','Storage media',2),(170,2,'7.11','Supporting utilities',0),(171,2,'7.12','Cabling security',0),(172,2,'7.13','Equipment maintenance',0),(173,2,'7.14','Secure disposal or re-use of equipment',0),(174,2,'8.1','User endpoint devices',2),(175,2,'8.2','Privileged access rights',0),(176,2,'8.3','Information access restriction',2),(177,2,'8.4','Access to source code',2),(178,2,'8.5','Secure authentication',2),(179,2,'8.6','Capacity management',0),(180,2,'8.7','Protection against malware',0),(181,2,'8.8','Management of technical vulnerabilities',2),(182,2,'8.9','Configuration management',0),(183,2,'8.10','Information deletion',0),(184,2,'8.11','Data masking',0),(185,2,'8.12','Data leakage prevention',0),(186,2,'8.13','Information backup',1),(187,2,'8.14','Redundancy of information processing facilities',1),(188,2,'8.15','Logging',2),(189,2,'8.16','Monitoring activities',0),(190,2,'8.17','Clock synchronization',0),(191,2,'8.18','Use of privileged utility program',2),(192,2,'8.19','Installation of software on operational systems',2),(193,2,'8.20','Networks security',2),(194,2,'8.21','Security of network services',2),(195,2,'8.22','Segregation of networks',0),(196,2,'8.23','Web filtering',0),(197,2,'8.24','Use of cryptography',2),(198,2,'8.25','Secure development life cycle',0),(199,2,'8.26','Application security requirements',2),(200,2,'8.27','Secure system architecture and engineering principles',2),(201,2,'8.28','Secure coding',2),(202,2,'8.29','Security testing in development and acceptance',2),(203,2,'8.30','Outsourced development',0),(204,2,'8.31','Separation of development, test and production environments',0),(205,2,'8.32','Change management',2),(206,2,'8.33','Test information',0),(207,2,'8.34','Protection of information systems during audit testing',2);
/*!40000 ALTER TABLE `controls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `en` text NOT NULL,
  `nl` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page` (`page`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_page_views`
--

DROP TABLE IF EXISTS `log_page_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_page_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` tinytext NOT NULL,
  `date` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_referers`
--

DROP TABLE IF EXISTS `log_referers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_referers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostname` tinytext NOT NULL,
  `url` text NOT NULL,
  `date` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_visits`
--

DROP TABLE IF EXISTS `log_visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_visits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `count` int(10) unsigned NOT NULL,
  `error` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `text` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--
-- ORDER BY:  `id`

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,NULL,'Dashboard','/dashboard'),(2,NULL,'Documentation','/documentation'),(3,NULL,'Risk matrix','/riskmatrix'),(4,NULL,'Links','/links'),(5,NULL,'Logout','/logout');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mitigation`
--

DROP TABLE IF EXISTS `mitigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mitigation` (
  `control_id` int(10) unsigned NOT NULL,
  `threat_id` int(10) unsigned NOT NULL,
  KEY `iso_measure_id` (`control_id`),
  KEY `threat_id` (`threat_id`),
  CONSTRAINT `mitigation_ibfk_1` FOREIGN KEY (`control_id`) REFERENCES `controls` (`id`),
  CONSTRAINT `mitigation_ibfk_2` FOREIGN KEY (`threat_id`) REFERENCES `threats` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mitigation`
--

LOCK TABLES `mitigation` WRITE;
/*!40000 ALTER TABLE `mitigation` DISABLE KEYS */;
INSERT INTO `mitigation` VALUES (1,1),(2,1),(12,1),(17,1),(103,1),(112,1),(113,1),(3,2),(5,2),(12,2),(13,2),(16,2),(18,2),(20,2),(112,2),(113,2),(7,59),(57,59),(58,59),(59,59),(82,59),(83,59),(84,59),(85,59),(91,59),(92,59),(93,59),(94,59),(95,59),(13,11),(26,11),(27,11),(31,11),(34,11),(61,11),(62,11),(68,11),(69,11),(73,11),(93,11),(114,11),(59,12),(89,12),(92,12),(106,12),(8,38),(9,38),(61,38),(71,38),(72,38),(73,38),(91,38),(92,38),(48,41),(51,41),(62,41),(93,41),(106,41),(57,40),(58,40),(70,40),(78,40),(82,40),(83,40),(84,40),(93,40),(60,39),(67,39),(78,39),(82,39),(83,39),(84,39),(85,39),(88,39),(89,39),(93,39),(7,56),(17,56),(58,56),(67,56),(68,56),(78,56),(82,56),(84,56),(87,56),(95,56),(7,42),(13,42),(30,42),(62,42),(8,17),(10,17),(16,17),(19,17),(43,17),(44,17),(45,17),(47,17),(48,17),(52,17),(53,17),(62,17),(11,18),(12,18),(14,18),(10,19),(44,19),(77,19),(91,19),(92,19),(8,37),(23,37),(25,37),(40,37),(53,37),(77,37),(10,20),(13,20),(14,20),(28,20),(30,20),(31,20),(37,20),(55,20),(56,20),(64,20),(75,20),(76,20),(80,20),(4,22),(10,22),(14,22),(15,22),(18,22),(28,22),(30,22),(65,22),(66,22),(77,22),(91,22),(92,22),(13,23),(31,23),(34,23),(36,23),(37,23),(13,24),(53,24),(55,24),(56,24),(72,24),(17,25),(18,25),(20,25),(21,25),(22,25),(26,25),(27,25),(74,25),(79,25),(90,25),(109,25),(17,26),(23,26),(24,26),(25,26),(52,26),(54,26),(57,53),(95,53),(6,29),(36,29),(50,29),(68,29),(71,29),(73,29),(88,29),(89,29),(93,29),(94,29),(6,28),(61,28),(64,28),(68,28),(73,28),(78,28),(82,28),(83,28),(84,28),(85,28),(88,28),(89,28),(93,28),(94,28),(114,28),(7,58),(70,58),(91,58),(92,58),(93,58),(94,58),(95,58),(9,30),(52,30),(53,30),(42,31),(43,31),(47,31),(48,31),(50,31),(61,31),(69,31),(14,32),(20,32),(21,32),(23,32),(25,32),(40,32),(74,32),(75,32),(76,32),(77,32),(79,32),(80,32),(93,32),(25,33),(40,33),(74,33),(76,33),(77,33),(79,33),(40,36),(74,36),(75,36),(79,36),(80,36),(91,36),(23,35),(62,35),(69,35),(109,35),(40,27),(41,27),(111,27),(93,7),(94,7),(95,7),(107,7),(8,8),(9,8),(13,8),(107,8),(111,8),(40,9),(41,9),(107,9),(111,9),(61,13),(63,13),(96,13),(97,13),(98,13),(99,13),(100,13),(16,14),(63,14),(64,14),(65,14),(66,14),(99,14),(100,14),(101,14),(102,14),(103,14),(104,14),(14,15),(96,15),(97,15),(101,15),(113,15),(42,44),(43,44),(44,44),(45,44),(46,44),(47,44),(48,44),(56,44),(42,45),(43,45),(44,45),(45,45),(48,45),(49,45),(56,45),(62,45),(103,45),(104,45),(105,45),(106,45),(42,48),(45,48),(48,48),(56,48),(62,48),(103,48),(104,48),(105,48),(106,48),(42,46),(45,46),(48,46),(49,46),(56,46),(62,46),(103,46),(104,46),(105,46),(106,46),(103,47),(104,47),(105,47),(49,49),(50,49),(106,49),(45,50),(48,50),(49,50),(50,50),(7,52),(57,52),(78,52),(95,52),(106,52),(7,43),(57,43),(78,43),(87,43),(95,43),(15,21),(17,21),(19,21),(26,21),(27,21),(28,21),(29,21),(30,21),(31,21),(32,21),(33,21),(35,21),(38,21),(39,21),(60,21),(3,3),(10,3),(11,3),(12,3),(13,3),(14,3),(115,1),(117,22),(118,1),(118,18),(120,28),(120,29),(121,11),(121,12),(121,28),(121,29),(122,59),(122,40),(122,39),(122,56),(122,42),(122,28),(122,58),(122,52),(122,43),(123,1),(123,56),(123,17),(123,21),(123,25),(123,26),(123,14),(124,22),(124,25),(125,17),(125,21),(126,25),(126,32),(127,25),(127,32),(128,20),(128,25),(128,32),(128,33),(128,36),(129,11),(129,21),(129,25),(130,20),(130,22),(130,21),(131,11),(131,20),(131,21),(131,23),(132,21),(133,59),(133,38),(133,19),(133,22),(133,58),(133,36),(134,59),(134,12),(134,38),(134,19),(134,22),(134,58),(135,59),(135,11),(135,41),(135,40),(135,39),(135,28),(135,29),(135,58),(135,32),(135,7),(136,59),(136,56),(136,28),(136,29),(136,58),(136,7),(136,52),(136,43),(136,53),(137,59),(137,12),(137,58),(137,30),(137,7),(137,52),(138,13),(138,15),(139,13),(139,14),(140,13),(140,14),(141,14),(141,15),(142,14),(143,1),(143,14),(143,44),(143,45),(143,48),(143,46),(143,47),(144,59),(144,45),(144,48),(144,46),(144,47),(144,49),(144,52),(144,43),(145,27),(145,7),(145,8),(145,9),(146,57),(147,25),(147,35),(149,1),(150,1),(150,11),(150,28),(150,15),(151,59),(151,40),(151,52),(151,43),(151,53),(152,17),(152,19),(152,20),(152,22),(153,18),(154,11),(154,42),(154,20),(154,23),(154,24),(154,8),(155,18),(155,20),(155,22),(155,32),(155,15),(156,22),(156,21),(157,19),(157,37),(157,22),(157,32),(157,33),(158,38),(158,30),(158,8),(159,13),(159,15),(160,31),(160,44),(160,45),(160,48),(160,46),(161,17),(161,31),(161,44),(161,45),(162,17),(162,19),(162,44),(162,45),(163,17),(163,19),(163,20),(163,44),(163,50),(164,17),(164,44),(164,45),(164,48),(164,46),(164,50),(165,44),(166,20),(166,24),(166,44),(166,45),(166,48),(166,46),(167,41),(167,17),(167,31),(167,44),(167,45),(167,48),(167,46),(167,50),(168,17),(168,37),(168,24),(168,30),(169,37),(169,26),(169,32),(169,33),(169,35),(170,45),(170,46),(170,49),(170,50),(171,29),(171,31),(171,49),(171,50),(172,41),(173,26),(174,38),(174,17),(174,37),(174,20),(174,24),(174,8),(175,42),(175,20),(175,22),(175,21),(176,21),(177,57),(177,21),(178,23),(178,29),(179,59),(179,12),(180,11),(180,38),(180,28),(180,31),(180,13),(181,11),(181,56),(181,28),(181,29),(182,40),(182,21),(183,17),(183,37),(183,26),(184,27),(185,42),(185,17),(185,37),(185,20),(185,22),(185,21),(185,23),(185,24),(185,25),(185,26),(185,28),(185,29),(185,31),(185,32),(185,33),(186,11),(186,41),(186,42),(186,17),(186,35),(186,45),(186,48),(186,46),(187,12),(187,41),(187,45),(187,48),(187,46),(187,49),(187,52),(188,20),(188,22),(188,28),(188,13),(188,14),(189,22),(189,28),(189,29),(189,13),(189,14),(190,22),(190,14),(191,21),(192,39),(192,56),(193,38),(193,29),(194,38),(194,24),(195,11),(195,38),(195,28),(195,29),(196,11),(196,42),(196,28),(197,37),(197,32),(197,33),(197,36),(197,27),(197,9),(198,57),(199,20),(199,25),(199,32),(199,33),(199,36),(200,59),(200,39),(200,28),(201,59),(201,39),(201,28),(201,43),(202,57),(202,39),(202,28),(202,29),(203,57),(203,56),(203,43),(204,57),(204,39),(204,21),(205,59),(205,40),(205,39),(205,56),(205,28),(206,57),(206,25),(207,40),(207,58),(116,2),(118,2),(119,2),(123,2),(124,2),(126,2),(149,2),(150,2),(154,2),(116,3),(118,3),(152,3),(153,3),(154,3),(155,3),(124,16),(146,16),(150,16),(153,16),(154,16),(155,16),(188,16),(192,16),(196,16),(39,57),(60,57),(81,57),(86,57),(87,57),(88,57),(89,57),(90,57),(108,57),(11,16),(13,16),(14,16),(18,16),(63,16),(64,16),(67,16),(69,16),(108,16),(113,16);
/*!40000 ALTER TABLE `mitigation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisations`
--

DROP TABLE IF EXISTS `organisations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisations`
--
-- ORDER BY:  `id`

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;
INSERT INTO `organisations` VALUES (1,'My organisation',1,'2022-01-01 00:00:00');
/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_access`
--

DROP TABLE IF EXISTS `page_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_access` (
  `page_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`page_id`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `page_access_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`),
  CONSTRAINT `page_access_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `language` varchar(2) NOT NULL,
  `layout` varchar(100) DEFAULT NULL,
  `private` tinyint(1) NOT NULL,
  `style` text DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `keywords` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `back` tinyint(1) NOT NULL,
  `form` tinyint(1) NOT NULL,
  `form_submit` varchar(32) DEFAULT NULL,
  `form_email` varchar(100) DEFAULT NULL,
  `form_done` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--
-- ORDER BY:  `id`

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'/documentation','en',NULL,0,'ol {\r\n  padding-left:25px;\r\n}\r\n\r\nol ol {\r\n  list-style-type: lower-alpha;\r\n}','Documentation','','','<h2>Performing a risk analysis with RAFIS</h2>\r\n<p>RAFIS is a tool that helps you to perform a risk analysis for information security. The goal is a selection of controls from the chosen security standard, based on threats that have been mapped out during a workshop. The use of RAFIS is based on a series of steps, as indicated below.</p>\r\n\r\n<ol>\r\n<li><a href=\"/documentation/bia\">Mapping relevant information systems within your organization.</a></li>\r\n<li><a href=\"/documentation/actors\">Identifying the actors that can breach your information security.</a></li>\r\n<li><a href=\"/documentation/riskanalysis\">Performing the actual risk analysis:</a>\r\n<ol>\r\n<li><a href=\"/documentation/scope\">Determining the scope of the risk analysis.</a></li>\r\n<li><a href=\"/documentation/interests\">Specifying the interests.</a></li>\r\n<li><a href=\"/documentation/threats\">Identifying the threats.</a></li>\r\n<li><a href=\"/documentation/controls\">Determining the mitigating controls.</a></li>\r\n<li><a href=\"/documentation/scenarios\">Working out possible scenarios.</a></li>\r\n<li><a href=\"/documentation/report\">Drawing up the report.</a></li>\r\n<li><a href=\"/documentation/progress\">Monitoring the progress of the control implementation.</a></li>\r\n</ol></li>\r\n</ol>\r\n\r\n<h2>Other functionalities</h2>\r\n<p>RAFIS contains a few more functionalities that are not directly related to performing a risk analysis. These are explained below.</p>\r\n<ul>\r\n<li><a href=\"/documentation/usermanagement\">Manage accounts for other users.</a></li>\r\n<li><a href=\"/documentation/datamanagement\">Managing your risk analysis information.</a></li>\r\n<li><a href=\"/documentation/advisor\">Working with RAFIS as an information security advisor.</a></li>\r\n</ul>',1,0,0,NULL,NULL,NULL),(2,'/documentation/bia','en',NULL,0,NULL,'Documentation - BIA','','','<p>During a business impact analysis (BIA), the relevant information systems of the organization are mapped. The way a system is important for the organization is determined for each system. Discuss the impact on the organization in the event of a problem with the availability, integrity and/or confidentiality of the system and the information stored therein.<p>\r\n\r\n<p>The system owner is the best person to determine the classification of the information stored in a system. This requires that an owner is designated for each information system.</p>\r\n\r\n<p>In the ideal situation, the BIA is done outside the risk analysis and the organization already has an overview of all information systems and in what way they are important for the organization. This step is then no more than copying the required information from the central information system overview. Unfortunately, reality often shows otherwise.</p>\r\n\r\n<p>In this step you do not have to limit yourself to the information systems that fall within the scope of an initial risk analysis. Preferably, enter all information systems that may fall within the scope of a future risk analysis. Choosing the scope of a risk analysis is done in a different step.</p>',1,1,0,NULL,NULL,NULL),(3,'/documentation/actors','en',NULL,0,NULL,'Documentation - Actors','','','<p>In this step, the actors that pose a threat to information security are identified. Their actions can inadvertently lead to an incident due to lack of knowledge or lack of interest in information security or because they deliberately and purposefully carry out a digital attack.</p>\r\n\r\n<p>Based on the stated willingness, level of knowledge and resources, RAFIS determines the threat level posed by an actor. If no resources are specified for an actor, RAFIS assumes that the actor has no malicious intent and that an incident caused by this actor was not intended.</p>\r\n\r\n<p>The overview of possible actors is a necessary aid in estimating the probability of a threat during the risk analysis. The available handout, which can be made via the scope page, contains this overview.</p>',1,1,0,NULL,NULL,NULL),(4,'/documentation/scope','en',NULL,0,NULL,'Documentation - Scope','','','<p>The first step that must be taken before starting a risk analysis is determining the scope. It is especially impractical for large organizations to perform a single risk analysis for the entire organization. It would then be wiser to perform multiple risk analyzes in which you focus on a smaller part of the organization for each risk analysis. </p>\r\n\r\n<p>If this is your first time performing a risk analysis, focus on the vital processes of the organization. The vital processes of an organization are the processes that ensure that the main products or services that an organization provides can actually be delivered. Take the information systems of a vital process as the scope for a risk analysis. Decide for yourself whether certain vital processes can be treated together in a single risk analysis.</p>\r\n\r\n<p>So, defining the scope means determining which information systems you include in the risk analysis. The scope can consist of the information systems belonging to, for example, a process, a department, a project or a collection of information systems that belong together for another reason. Be careful about choosing your scope too wide. The danger of a scope that is too broad is that you do not go deeply enough into important details and therefore get a too superficial picture of the actual risks.</p>',1,1,0,NULL,NULL,NULL),(5,'/documentation/riskanalysis','en',NULL,0,NULL,'Documentation - Risk analysis','','','<h2>The preparation</h2>\r\n<p>It is very important to realize that during a risk analysis, knowledge about which risks an organization runs cannot arise from a tool or a method, but only from the people who are present during the risk analysis. They are the ones who know what is going on. A risk analysis is no more than a way to collect this knowledge in a structured way. The risk analysis stands or falls with the selection of the participants. So look for people who have a good idea of what is really important for the organization, but who also have sufficient insight into what is going on in the workplace. Look for people who are responsible for things that fall within the chosen scope, people who directly experience the disadvantages of problems that occur within the chosen scope. You should realize that people who are good at estimating the probability of an incident, are not necessarily the people who can also estimate the correct impact and vice versa. Often people from the business are better at estimating the impact and techies better at estimating the opportunity.</p>\r\n\r\n<p>Prior to the risk analysis, an amount must be linked to the values for impact. This is not fixed in RAFIS, because this is different for every organization. A loss of €10,000 can be a large amount for a small company and an insignificant amount for a multinational. These values are best determined with someone with solid knowledge of the financial situation of the organization. It is important to realize that these values are not intended to link a claim amount to the ultimate risks, but only to be able to properly place the impact of a risk in relation to the impact of the other risks. The impact per risk therefore does not have to be demonstrated with a calculation or hard figures. A well-founded feeling is sufficient. A well-considered impact is also important in order to be able to repeat a risk analysis at a later time and to compare the results with the analysis performed earlier.</p>\r\n\r\n<h2>A first meeting</h2>\r\n<p>A good risk analysis is not something you can do in ten minutes. The time required for this is closer to half a day. The actual time required naturally depends on the chosen scope, the number of participants and their experience in performing a risk analysis. It is therefore important that the participants are well aware of what is expected of them. Discuss the process with them and give them an idea of the kind of questions that await them. Only conduct a risk assessment with people who are willing to put in this amount of time and energy, otherwise it will be a waste of effort.</p>\r\n\r\n<p>Although the scope of the risk analysis can be a process or a department, you perform the risk analysis on the associated information and information systems. Because although a risk can arise from, for example, a wrongly organized or missing process, the risks with regard to information must be considered. After all, we are talking about information security here and not process security.</p>',1,1,0,NULL,NULL,NULL),(6,'/documentation/threats','en',NULL,0,NULL,'Documentation - Threat analysis','','','<p>During the threat analysis, the threats that are relevant to the organization are identified. This involves determining the probability that a threat will lead to an incident and the impact of such an incident. It must then be determined how to deal with the identified risk. RAFIS has a list of general threats (templates). The use of this list is of course not mandatory. The list is intended as a source of inspiration. If an ISO 27001 certification is the goal, going through this entire template list is worth considering.</p>\r\n\r\n<p>A handout can be made at the end of setting up the scope. This explanation assumes that the participants have this handout.</p>\r\n\r\n<p>The first thing you determine when dealing with a threat is the possible actor. Find out which actor benefits from gaining access to information or harming the organization. Not every threat has an actor. Sometimes an incident occurs because you are just unlucky. For example, hardware sometimes breaks. If several actors are possible, then assume the most threatening actor.</p>\r\n\r\n<p> When determining the probability, start from the threat posed by the chosen actor and the (possible) presence of vulnerabilities in the systems concerned. The translation of the threat from the actor to the chance factor can be determined as indicated on the handout. However, this is the probability associated with the gross risk. What we are looking for is the opportunity that takes into account the measures already taken. This is the net risk. The goal is that ultimately the residual risk can be accepted for every threat because adequate measures have been taken.</p> \r\n\r\n<p>When reducing the probability as a result of measures taken, RAFIS proposes the following rule. If you have taken enough measures, go down a maximum of two levels in the chance. If measures have been taken, but there is room for improvement, go down one level at most. The reason for this is that it is not realistic to think that an advanced actor targeting you can be kept out. If you want to further reduce the risk, you must therefore take impact-reducing measures. It often turns out that once an actor is inside, he has free rein. Too many organizations take too few impact-reducing measures. With this rule, RAFIS tries to do something about it. You are of course free to deviate from this rule.</p>\r\n\r\n<p>If the threat does not require an actor or if the actor is not a malicious actor, see how often an incident could occur as a result of the threat.</p>\r\n\r\n<p>The impact is determined by all possible consequences of an incident. Think of the possible consequences for the availability, integrity and confidentiality of the affected information, image damage, financial damage or administrative / political consequences.</p>\r\n\r\n<p>Include low-risk threats in the overview, even if you accept the risk. These may be useful when drawing up a scenario in a next step.</p>\r\n\r\n<p>It is good to realize that a risk is usually not a dot on the risk matrix, but a line. A risk can manifest itself with a small impact or with a large impact. Low-impact risks tend to be more likely than high-impact risks. Take, for example, receiving phishing mail. A mail server receives many phishing emails every day, but most of them are blocked by filters. Sometimes some relatively harmless phishing mail gets through, but users see through it or the underlying web form has already been removed by the hosting party. Very occasionally it hits the spot and users fill in really confidential information. These situations can be plotted in a line on the risk matrix. It is possible to lower the entire line with measures, but you may want to focus first on the middle part of the line and then on the right part of the line.</p>\r\n\r\n<p>In the approach, \'control\' means lowering the probability as well as the impact, \'avoid\' means lowering the probability, \'resist\' means lowering the impact and \'accept\' means not taking further action to mitigate the risk. to lower. Choosing \'accept\' only makes sense if you choose to work through the entire list of threat templates.</p>\r\n\r\n<p>Three entry fields are available per threat; \'Desired situation / actions to be taken\', \'Current situation / current measures\' and \'Argumentation for the choice made\'. These fields can be used for a baseline measurement, the later action plan and argumentation about the chosen chance, impact and approach, respectively. The argumentation is important information for any certification. The content of these fields is therefore more important than the chance, impact and approach fields. In fact, the latter indicate no more than a prioritization or urgency.</p>',1,1,0,NULL,NULL,NULL),(7,'/documentation/controls','en',NULL,0,NULL,'Documentation - Controls','','','<p>After the threats have been identified, appropriate measures must be chosen for each threat. The available measures come from the standard chosen for this case, for example NEN-ISO/IEC 27002. It is advisable to keep the chosen standard for this step. It is wise to perform this step with or by someone with sufficient knowledge of the chosen standard.</p>\r\n\r\n<p>A control can be chance-reducing, impact-reducing or both. Depending on the approach chosen for a threat, controls are in line with it or not. For example, if one has opted for an evasive approach (reducing the chance), the impact-reducing control are not in line with this. These can still be selected, but the text of the control has been crossed out to make clear that the measure is not in line.</p>\r\n\r\n<p> At the top of the control list is the name of the standard from which these controls originate. To the right of it is a cross symbol. If you click on it, a pulldown appears with the threat templates present in RAFIS. Selecting one of these threats ensures that the, according to RAFIS, mitigating controls are accentuated. The current selection, however, is not adjusted.</p>\r\n\r\n<p>An exclamation mark icon indicates a conflict. This is if you select controls for a threat that you have accepted or if you have not yet selected controls for a threat that you have not accepted.</p>',1,1,0,NULL,NULL,NULL),(8,'/documentation/report','en',NULL,0,NULL,'Documentation - Report','','','<p> Once the risk analysis has been completed, the report can be drawn up. The report contains an overview of the risk analysis performed and does not express an opinion on the result.</p>',1,1,0,NULL,NULL,NULL),(9,'/documentation/progress','en',NULL,0,NULL,'Documentation - Progress','','','<p>The last and optional step that RAFIS offers is monitoring the progress of the measure implementation. Tasks can be assigned for each control. The person to whom a task is assigned, of course, does not necessarily have to perform the task himself, but only have to make sure that it is carried out. A second person can be appointed to carry out an independent assessment as far as possible on the correct implementation of the measure.</p>\r\n\r\n<p>When recording a task, you can choose to send notifications by e-mail. RAFIS sends a reminder of the task by e-mail when the deadline has passed. The reviewer of a controle can mark a task as \'finished\' via a link in the received mail.</p>',1,1,0,NULL,NULL,NULL),(10,'/documentation/datamanagement','en',NULL,0,NULL,'Documentation - Data management','','','<h2>Export your data</h2>\r\n<p>The data management section in RAFIS offers you to secure your risk analyzes by means of an export. The information that is included in the export is the information systems from the BIA, the actors and the risk analyses. The user accounts are not included.</p>\r\n\r\n<p>The export is a GZip file containing a copy of the data in JSON. The data is digitally signed, which is verified during an import. This is to protect the integrity of the database during an import.</p>\r\n\r\n<h2>Import your data</h2>\r\n<p>Exported data can of course be imported again. The data from the progress section may contain references to users. Because users are not included in the export, they must be created first. A list of non-existent users is shown after the import and also listed in the information field of a task. You can choose to create the missing users and re-import the data.</p>',1,1,0,NULL,NULL,NULL),(11,'/documentation/advisor','en',NULL,0,NULL,'Documentation - Advisor','','','<p>RAFIS can also be used by information security consultants. Both the advisor and the client create their own RAFIS account for their own organization. The advisor sends a request to the client via the advisor module to gain access to his risk analyses. If that request is approved by the client, the adviser can temporarily switch to the client\'s organization via the adviser module. The advisor has access to the BIA, the actors and the risk analyzes of the client. Both the advisor and the client can revoke this permission.</p>\r\n\r\n<p>Approving a request for access can only be done by someone with the Administrator role. In the CMS, accessible for an administrator via the CMS link in the bar at the bottom, requests can be approved and denied and permissions given can be revoked via the Advisors module.</p>',1,1,0,NULL,NULL,NULL),(12,'/documentation/usermanagement','en',NULL,0,NULL,'Documentation - User management','','','<p>It is possible for a user with the Administrator role to manage other users within the same organization. To do so, an administrator clicks on the CMS link in the bar at the bottom.</p>\r\n\r\n<p>Using the Users module, accounts for other users can be created, modified or deleted. The Access module provides an overview of the rights of the different users and the possible roles. The Verifier role is for people who need to be able to approve tasks assigned in the Progress module, but don\'t want or aren\'t allowed to have more access rights in the RAFIS website.</p>',1,1,0,NULL,NULL,NULL),(13,'/documentation/interests','en',NULL,0,NULL,'Documentation - Interests','','','<p>The information within the scope and the processes supported by this information serve one or more interests. It is good to document these interests clearly and completely. This helps determining the impact. It also helps to clarify the importance of information security within the organization. Information security is not an isolated thing. It supports the achievement of organizational goals.</p>\r\n\r\n<p>When determining the interests, think of it in terms of business goals and image, but also the interests of any other third party.</p>',1,1,0,NULL,NULL,NULL),(14,'/documentation/scenarios','en',NULL,0,NULL,'Documentation - Scenarios','','','<p>A major incident is often not the result of a single vulnerability, but a series of incidents. For example, the hack in the port of Rotterdam started with downloading a software update infected with malware, because the supplier had been hacked. Via an international internal network (network segmentation and internal firewalls were missing), the malware was able to reach the systems in the port of Rotterdam and actually infect it due to the lack of security updates.</p>\r\n\r\n<p>In this step, think about how multiple threats combined together can lead to a major incident. Such a scenario may feel somewhat Hollywood-esque, but so were the descriptions of the Diginotar hack and the attack on the port of Rotterdam beforehand. A scenario is not about whether it is probable, but whether it is technically possible.</p>\r\n\r\n<p>The threat assessment documentation recommends including low-risk threats. These are possible stepping stones to a major incident. So, also think about these risks to determine whether they can become part of a scenario.</p>\r\n\r\n<p> By devising one or more scenarios based on the specific threats identified, the seriousness of the individual threats and vulnerabilities can be made more understandable. This makes it easier to draw attention to the necessity of the mitigating measures and the investments that may be necessary for this.</p>',1,1,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reroute`
--

DROP TABLE IF EXISTS `reroute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reroute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original` varchar(100) NOT NULL,
  `replacement` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `description` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `risk_assess_sessions`
--

DROP TABLE IF EXISTS `risk_assess_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risk_assess_sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `access_code` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `risk_assess_values`
--

DROP TABLE IF EXISTS `risk_assess_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risk_assess_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `risk_assess_session_id` int(10) unsigned NOT NULL,
  `chance` tinyint(3) unsigned NOT NULL,
  `impact` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `risk_assess_session_id` (`risk_assess_session_id`),
  CONSTRAINT `risk_assess_values_ibfk_1` FOREIGN KEY (`risk_assess_session_id`) REFERENCES `risk_assess_sessions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `non_admins` smallint(6) NOT NULL,
  `session` tinyint(1) NOT NULL,
  `cms` tinyint(1) NOT NULL,
  `cms/access` tinyint(1) NOT NULL,
  `cms/action` tinyint(1) NOT NULL,
  `cms/analytics` tinyint(1) NOT NULL,
  `cms/apitest` tinyint(1) NOT NULL,
  `cms/file` tinyint(1) NOT NULL,
  `cms/menu` tinyint(1) NOT NULL,
  `cms/organisation` tinyint(1) NOT NULL,
  `cms/page` tinyint(1) NOT NULL,
  `cms/role` tinyint(1) NOT NULL,
  `cms/settings` tinyint(1) NOT NULL,
  `cms/user` tinyint(1) NOT NULL,
  `cms/language` tinyint(1) NOT NULL,
  `cms/reroute` tinyint(4) DEFAULT 0,
  `bia` tinyint(4) DEFAULT 0,
  `dashboard` tinyint(4) DEFAULT 0,
  `account` tinyint(4) DEFAULT 0,
  `cms/validate` tinyint(4) DEFAULT 0,
  `cms/statistics` tinyint(4) DEFAULT 0,
  `data` tinyint(4) DEFAULT 0,
  `cms/advisor` tinyint(4) DEFAULT 0,
  `advisor` tinyint(4) DEFAULT 0,
  `actors` tinyint(4) DEFAULT 0,
  `case` tinyint(4) DEFAULT 0,
  `case/interests` tinyint(4) DEFAULT 0,
  `case/scope` tinyint(4) DEFAULT 0,
  `case/threats` tinyint(4) DEFAULT 0,
  `case/risk` tinyint(4) DEFAULT 0,
  `case/controls` tinyint(4) DEFAULT 0,
  `case/scenarios` tinyint(4) DEFAULT 0,
  `case/report` tinyint(4) DEFAULT 0,
  `case/progress` tinyint(4) DEFAULT 0,
  `case/progress/report` tinyint(4) DEFAULT 0,
  `case/progress/export` tinyint(4) DEFAULT 0,
  `cms/control` tinyint(4) DEFAULT 0,
  `cms/standard` tinyint(4) DEFAULT 0,
  `cms/threat` tinyint(4) DEFAULT 0,
  `cms/threat/categories` tinyint(4) DEFAULT 0,
  `cms/threat/export` tinyint(4) DEFAULT 0,
  `cms/control/categories` tinyint(4) DEFAULT 0,
  `cms/control/export` tinyint(4) DEFAULT 0,
  `case/progress/done` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--
-- ORDER BY:  `id`

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrator',0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1),(2,'Manager',1,1,1,1,0,0,0,0,0,0,0,0,0,1,0,0,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0),(3,'User',1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0),(5,'Advisor',0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0),(6,'Controller',1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` varchar(128) NOT NULL,
  `login_id` varchar(128) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `expire` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `bind_to_ip` tinyint(1) NOT NULL,
  `name` tinytext DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `type` varchar(8) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--
-- ORDER BY:  `id`

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'admin_page_size','integer','25'),(2,'database_version','integer','3'),(3,'default_language','string','en'),(8,'head_description','string','Free and open method for performing a risk analysis for information security, using ISO/IEC 27002 for control selection.'),(9,'head_keywords','string','risicoanalyse, informatiebeveiliging, cyber security, informatieveiligheid, dreigingen, ISO 27002, ISO 27001, NEN 7510, BIO, maatregelen, business impact analyse, gratis methodiek, open methodiek'),(10,'head_title','string','RAFIS'),(11,'hiawatha_cache_default_time','integer','3600'),(12,'hiawatha_cache_enabled','boolean','false'),(27,'secret_website_code','string',''),(28,'session_persistent','boolean','true'),(29,'session_timeout','integer','86400'),(30,'start_page','string','dashboard'),(33,'webmaster_email','string','root@localhost'),(45,'default_standard','integer','2'),(46,'validate_import_signature','boolean','true'),(47,'advisor_role','string','Advisor');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standards`
--

DROP TABLE IF EXISTS `standards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `standards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standards`
--
-- ORDER BY:  `id`

LOCK TABLES `standards` WRITE;
/*!40000 ALTER TABLE `standards` DISABLE KEYS */;
INSERT INTO `standards` VALUES (1,'ISO/IEC 27002:2017',1),(2,'ISO/IEC 27002:2022',1);
/*!40000 ALTER TABLE `standards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `threat_categories`
--

DROP TABLE IF EXISTS `threat_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `threat_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `threat_categories`
--
-- ORDER BY:  `id`

LOCK TABLES `threat_categories` WRITE;
/*!40000 ALTER TABLE `threat_categories` DISABLE KEYS */;
INSERT INTO `threat_categories` VALUES (1,'Responsibility'),(2,'Laws and regulations'),(3,'Continuity and reliability of systems'),(4,'Incident handling'),(5,'Access to information'),(6,'Exchanging and storing information'),(8,'Human behaviour'),(9,'Physical security'),(10,'Business continuity');
/*!40000 ALTER TABLE `threat_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `threats`
--

DROP TABLE IF EXISTS `threats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `threats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL,
  `threat` text NOT NULL,
  `description` text NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `confidentiality` enum('p','s','-') NOT NULL,
  `integrity` enum('p','s','-') NOT NULL,
  `availability` enum('p','s','-') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `threats_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `threat_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `threats`
--
-- ORDER BY:  `id`

LOCK TABLES `threats` WRITE;
/*!40000 ALTER TABLE `threats` DISABLE KEYS */;
INSERT INTO `threats` VALUES (1,1,'Lack of direction on information security from management.','The management does not focus on information security. Responsibilities towards line managers are not assigned. An information security policy and/or ISMS is missing.',1,'p','p','p'),(2,2,'Line managers do not take their responsibility for information security.','Line managers do not sufficiently ensure that information security is implemented correctly within their department. The ownership of information systems is not well invested. Security is not a permanent part of projects.',1,'p','p','p'),(3,4,'Employees do not act according to what is expected of them.','The employees lack awareness towards information security and do not feel the need to contribute to it.',1,'p','p','p'),(7,36,'Legislation on information in the cloud.','Legislation in some countries allows the government of such a country to view information stored in the cloud.',2,'p','-','-'),(8,37,'Foreign law when visiting a country.','Legislation in some countries allows the government to require access to the data on systems included when visiting that country.',2,'p','-','-'),(9,38,'Legislation on the use of cryptography.','Legislation in some countries allows governments to demand a copy of cryptographic keys.',2,'p','-','-'),(11,6,'Access to information is blocked.','Information on a system has been made inaccessible because malware (ransomware, wipers) or an attacker has encrypted or deleted this information.',3,'-','-','p'),(12,7,'Network services are overloaded.','A network service is reduced in availability due to a malicious attack (DoS) or due to an unforeseen increase in the amount of requests or the resources required to handle a request. Requests can come from users, but also from other systems.',3,'-','-','p'),(13,39,'Incidents are not dealt with in a timely manner.','The consequences of incidents are unnecessarily large as a result. Within the company there is insufficient network monitoring and there is no central reporting point for security incidents.',4,'p','s','-'),(14,40,'Information for dealing with incidents is lacking.','System administrators do not have enough technical information about the problem to solve it. There is no action plan, which means that the incident continues unnecessarily long.',4,'p','p','p'),(15,41,'Recurrence of incidents.','Causes of incidents are not held accountable for their actions. Managers have insufficient insight into recurring incidents, so that they do not manage them.',4,'p','s','p'),(16,14,'Systems are not used for their intended purpose.','The lack of a policy on internet use, for example, increases the risk of abuse.',8,'s','-','p'),(17,15,'Taking away company assets.','Due to insufficient checks on the issue and incorrect inventory of company assets, there is a chance that the theft will not be noticed or will be noticed too late.',8,'s','-','p'),(18,16,'Policy is not followed by lack of sanctions.','Due to the lack of sanctions for violating rules, there is a chance that employees will not take the policy measures seriously.',8,'p','s','-'),(19,17,'Allowing external parties into the building or onto the network.','The admission of external parties, such as suppliers and project partners, can have consequences for the confidentiality of the information available within the premises or via the network.',8,'p','-','-'),(20,19,'Abuse of someone else\'s identity.','Due to insufficient (possibility of) checking an identity, unauthorized access can be obtained to confidential information. This also includes social engineering, such as phishing and CEO fraud.',8,'p','p','-'),(21,21,'Access rights set incorrectly.','Due to a missing, incorrect or unclear process for allocating and taking rights, a person can inadvertently have more rights. These rights can be abused by this person or by others (eg via malware).',8,'p','p','-'),(22,20,'Abuse of special rights.','Due to insufficient control of employees with special rights, such as system administrators, there is a risk of unauthorized access to sensitive information.',8,'p','p','-'),(23,22,'Bad password usage.','Lack of password policies and employee awareness can lead to the use of weak passwords, the writing of passwords, or the use of the same password across multiple systems.',5,'p','p','-'),(24,23,'Leaving workplaces unattended.','In the absence of a clear-desk and/or clear-screen policy, access can be gained to sensitive information.',5,'p','s','-'),(25,24,'Uncertainty about classification and powers.','Due to a lack of clarity about the confidentiality of information and authority of persons, there is a risk of unauthorized access to sensitive information.',5,'p','s','-'),(26,25,'Information on systems upon repair or disposal.','Sensitive information may leak if storage media or systems containing storage media are discarded or offered to third parties for repair.',5,'p','-','-'),(27,35,'Misuse of cryptographic keys and/or use of weak algorithms.','There is a risk of misuse of cryptographic keys due to incorrect or missing key management. The use of weak cryptographic algorithms provides a false sense of security.',6,'p','p','-'),(28,26,'Exploitation of vulnerabilities in applications or hardware.','Abuse of vulnerabilities in applications or hardware. Vulnerabilities in applications or hardware are misused (exploits) to gain unauthorized access to an application and the information stored therein.',5,'p','p','-'),(29,27,'Exploiting network security vulnerabilities.','Weaknesses in the security of the (wireless) network are misused to gain access to this network.',5,'p','p','-'),(30,29,'Information outside the protected environment.','Information that is taken outside the office for permitted use, for example, is no longer properly protected. Also consider Bring Your Own Device (BYOD).',5,'p','-','-'),(31,30,'Eavesdropping equipment.','Sensitive information is retrieved by means of keyloggers or network taps.',5,'p','-','-'),(32,31,'Sending sensitive information insecurely.','Breach of confidentiality of information by sending information unencrypted.',6,'p','s','-'),(33,32,'Sending sensitive information to incorrect person.','Breach of confidentiality of information due to insufficient control of recipient.',6,'p','-','-'),(35,33,'Loss of information due to expiration of the shelf life of the storage method.','Information is lost due to the medium becoming unreadable or the file format becoming outdated.',6,'-','-','p'),(36,34,'Incorrect information.','Unwanted actions as a result of incorrect company information or receiving incorrect information. This could be as a result of willful act or a mistake.',6,'-','p','-'),(37,18,'Loss of mobile devices and storage media.','The loss of mobile devices and storage media can lead to a breach of the confidentiality of sensitive information.',8,'p','-','s'),(38,8,'Attacks via systems that are not under their own control.','Due to insufficient control over the security of private and home equipment and other equipment from third parties, there is a risk of contamination with malware, for example.',3,'-','-','p'),(39,11,'Failure of systems due to software errors.','Errors in software can lead to system crashes or corruption of information stored in the system.',3,'-','p','p'),(40,10,'Failure of systems due to configuration errors.','Incorrect configuration of an application can lead to incorrect processing of information.',3,'-','p','s'),(41,9,'Failure of systems due to hardware errors.','Insufficient quality hardware can lead to system failure.',3,'-','-','p'),(42,13,'User errors.','Insufficient knowledge or too little control over other people\'s work increases the risk of human errors. User interfaces that are not tailored to the user level increase the chance of errors.',8,'-','p','s'),(43,50,'Software is no longer supported by the publisher.','Security patches will no longer be issued for software that is no longer supported. Also think of Excel and Access applications.',10,'-','-','p'),(44,42,'Unauthorized physical access.','The lack of access passes, visibility of entrances and awareness among employees increases the chance of unauthorized physical access.',9,'p','-','-'),(45,43,'Fire.','The lack of fire detectors and fire extinguishing equipment increases the consequences of a fire.',9,'-','-','p'),(46,45,'Flooding.','Flooding and flooding can damage computers and other business assets.',9,'-','-','p'),(47,46,'Pollution of the environment.','Contamination of the environment can lead to the organization being (temporarily) unable to work.',9,'-','-','p'),(48,44,'Explosion.','Explosions can cause damage to the building and equipment and casualties.',9,'-','-','p'),(49,47,'Failure of facility resources (gas, water, electricity, air conditioning).','Failure of facility resources can mean that one or more business units can no longer do their job.',9,'-','-','p'),(50,48,'Vandalism.','Damage to or destruction of company property as a result of an undirected action, such as vandalism or rodents.',9,'-','-','p'),(52,49,'Unavailability of third-party services.','The unavailability of third-party services due to system failure, bankruptcy, unplanned contract termination or unacceptable changes in services (for example, due to a company takeover).',10,'-','-','p'),(53,51,'Losing important knowledge when employees are unavailable.','Employees who leave the company or who cannot be deployed for a long time due to an accident possess knowledge that is therefore no longer available.',10,'-','-','p'),(56,12,'Errors due to changes in other systems.','Errors arise in a system as a result of changes in linked systems.',3,'-','p','p'),(57,5,'Insufficient attention to security during software development.','Insufficient attention to security when developing software yourself or having it developed leads to a breach of information security.',3,'p','p','p'),(58,28,'Insufficient attention to security when outsourcing work.','Because external parties / suppliers do not have their information security in order, infringements can occur on the information to which they have access.',5,'p','-','-'),(59,3,'Insufficient attention to security within projects.','Insufficient attention to security within projects. Within projects (excluding software development) there is insufficient attention to security. This has negative consequences for new systems and processes that are introduced within the organization.',1,'p','s','p');
/*!40000 ALTER TABLE `threats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `organisation_id` int(10) unsigned NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password` tinytext NOT NULL,
  `status` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `authenticator_secret` varchar(16) DEFAULT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `crypto_key` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `organisation_id` (`organisation_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `organisations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--
-- ORDER BY:  `id`

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','none',1,NULL,'Administrator','root@localhost',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-17 12:16:31
