<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="standards">
<div class="standard">
<xsl:choose>
<xsl:when test="count(standard)>1">
<form action="/{/output/page}" method="post">
<input type="hidden" name="submit_button" value="standard" />
Standard: <select name="standard" onChange="javascript:submit()" class="text">
<xsl:for-each select="standard">
<option value="{@id}"><xsl:if test="@selected='yes'"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</form>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="standard" />
</xsl:otherwise>
</xsl:choose>
</div>
</xsl:template>

</xsl:stylesheet>
