<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Matrix template
//
//-->
<xsl:template match="matrix">
<p>The risk matrix as being used by RAFIS is as follows:</p>
<table class="matrix">
<tr><td></td><td></td><td colspan="{count(row)-1}">Impact</td></tr>
<xsl:for-each select="row">
<tr>
	<xsl:if test="position()=1"><td></td></xsl:if>
	<xsl:if test="position()=2"><td rowspan="{count(../row)-1}" class="chance">Chance</td></xsl:if>
	<xsl:for-each select="cell">
		<td><xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if><xsl:value-of select="." /></td>
	</xsl:for-each>
</tr>
</xsl:for-each>
</table>

<p>RAFIS uses the following approaches to deal with a risk:</p>
<ul>
<li>Control: Taking measures to lower the impact and chance of an incident.</li>
<li>Avoid: Taking measures to lower the chance of an incident.</li>
<li>Resist: Taking measures to lower the impact of an incident.</li>
<li>Accept: Accepting the impact and chance of an incident.</li>
</ul>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Risk matrix</h1>
<xsl:apply-templates select="matrix" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
