<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../banshee/main.xslt" />
<xsl:include href="../banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-stiped table-hover">
<thead>
<tr>
<th>Number</th>
<th>Name</th>
<th>Reduces</th>
<th class="links">Links</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="controls/control">
<xsl:if test="category">
<tr class="category">
<td colspan="4"><xsl:value-of select="category" /></td>
</tr>
</xsl:if>
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="reduce" /></td>
<td><xsl:value-of select="links" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group left">
<a href="/{/output/page}/new" class="btn btn-default">New control</a>
<a href="/cms" class="btn btn-default">Back</a>
<a href="/{/output/page}/categories" class="btn btn-default">Control categories</a>
<a href="/{/output/page}/export" class="btn btn-default">Export</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="control/@id">
<input type="hidden" name="id" value="{control/@id}" />
</xsl:if>

<label for="number">Number</label>
<input type="text" id="number" name="number" value="{control/number}" class="form-control" />
<label for="name">Name:</label>
<input type="text" id="name" name="name" value="{control/name}" class="form-control" />
<label for="reduce">Reduces:</label>
<select id="reduce" name="reduce" class="form-control"><xsl:for-each select="reduces/reduce">
<option value="{position()-1}"><xsl:if test="(position()-1)=../../control/reduce"><xsl:attribute name="selected" value="selected" /></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each></select>

<h2>Threats</h2>
<div class="threats">
<xsl:for-each select="threats/threat">
<div class="threat">
<div class="title"><input type="checkbox" name="threat_links[]" value="{@id}"><xsl:if test="@checked='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input><span onClick="javascript:$('#desc_{@id}').slideToggle(0)"><xsl:value-of select="title" /></span></div>
<div class="description" id="desc_{@id}"><xsl:value-of select="description" /></div>
</div>
</xsl:for-each>
</div>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save control" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="control/@id">
<input type="submit" name="submit_button" value="Delete control" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<div class="standard"><xsl:value-of select="standard" /></div>
<img src="/images/icons/controls.png" class="title_icon" /><h1>Control administration</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
