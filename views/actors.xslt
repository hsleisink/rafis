<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />
<xsl:include href="banshee/pagination.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs actors">
<thead class="table-xs">
<tr><th>Actor</th><th>Willingness / chance</th><th>Knowledge level</th><th>Resources</th><th>Threat</th></tr>
</thead>
<tbody>
<xsl:for-each select="actors/actor">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{@id}'">
<td><span class="table-xs">Actor</span><xsl:value-of select="name" /></td>
<td><span class="table-xs">Willingness / chance</span><xsl:value-of select="chance" /></td>
<td><span class="table-xs">Knowledge level</span><xsl:value-of select="knowledge" /></td>
<td><span class="table-xs">Resources</span><xsl:value-of select="resources" /></td>
<td><span class="table-xs">Threat</span><xsl:value-of select="threat" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="right">
<xsl:apply-templates select="pagination" />
</div>

<div class="btn-group">
<a href="/{/output/page}/new" class="btn btn-default">New actor</a>
<a href="/dashboard" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<xsl:if test="actor/@id">
<input type="hidden" name="id" value="{actor/@id}" />
</xsl:if>

<label for="name">Actor's name:</label>
<input type="text" id="name" name="name" value="{actor/name}" class="form-control" />
<label for="chance">Willingness to attack or chance of causing an incident:</label>
<select name="chance" class="form-control">
<xsl:for-each select="chance/item"><option value="{position()}"><xsl:if test="position()=../../actor/chance"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="knowledge">Knowledge level:</label>
<select name="knowledge" class="form-control">
<xsl:for-each select="knowledge/item"><option value="{position()}"><xsl:if test="position()=../../actor/knowledge"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="resources">Resources:</label>
<select name="resources" class="form-control">
<xsl:for-each select="resources/item"><option value="{position()}"><xsl:if test="position()=../../actor/resources"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option></xsl:for-each>
</select>
<label for="reason">Target of attack or cause of incident:</label>
<input type="text" id="reason" name="reason" value="{actor/reason}" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Save actor" class="btn btn-default" />
<a href="/{/output/page}" class="btn btn-default">Cancel</a>
<xsl:if test="actor/@id">
<input type="submit" name="submit_button" value="Delete actor" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/actors.png" class="title_icon" />
<xsl:apply-templates select="breadcrumbs" />
<h1>Actors</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
<div id="help">
<p>On this page you indicate the actors who can cause a specific security incident. An incident can be the result of a deliberate action, where you have to look at the willingness of an actor to actually attack you. The knowledge level indicates the actor's knowledge of cybersecurity and digital attacks.</p>
<p>An incident can also occur as a result of an unintended action. You do not look at the willingness of the actor, but the chance that an actor will cause an incident. Think of the cause of (deadline) stress, insufficient management or policy or a lack of interest or awareness. Because an actor in such a case does not deploy any resources, select 'n/a' for resources.</p>
<p>It is allowed to name an actor several times, choosing different values for chance/willingness and level of knowledge. For instance:</p>
<table class="table table-condensed table-striped">
<thead>
<tr><th>Actor</th><th>Willingeness / chance</th><th>Knowledge level</th><th>Resources</th></tr>
</thead>
<tbody>
<tr><td>Employees</td><td>minor</td><td>basic</td><td>n/a</td></tr>
<tr><td>Employees (ICT)</td><td>minor</td><td>skilled</td><td>n/a</td></tr>
<tr><td>Employees (dissatisfied)</td><td>possible</td><td>basic</td><td>limited</td></tr>
</tbody>
</table>
<p>In the first step of the risk analysis, you determine the scope of the risk analysis. This step offers you the opportunity to make a handout for those present at the risk analysis where this scope can be seen. These actors are also mentioned on that handout.</p>
</div>
</xsl:template>

</xsl:stylesheet>
