<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />

<table class="table table-condensed">
<thead>
<tr><th>Organisation</th><th>Status</th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="role">
<tr>
<td><xsl:value-of select="." /></td>
<td><xsl:if test="@ready='no'">Requested</xsl:if><xsl:if test="@ready='yes'"><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="Activate" class="btn btn-primary btn-xs" /></form></xsl:if><xsl:if test="@ready='active'"><form action="/{/output/page}" method="post"><input type="submit" name="submit_button" value="Deactivate" class="btn btn-info btn-xs" /></form></xsl:if></td>
<td><form action="/{/output/page}" method="post"><input type="hidden" name="id" value="{@id}" /><input type="submit" name="submit_button" value="Remove" class="btn btn-danger btn-xs" onClick="javascript:return confirm('REMOVE: Are you sure?')" /></form></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2>Request advisor role</h2>
<p>To request the advisor role for an organization, please provide below the RAFIS username or email address of your contact person at the organization for which you wish to act as an advisor.</p>
<form action="/{/output/page}" method="post" class="request">
<div class="input-group">
<input type="text" id="user" name="user" placeholder="Username or e-mail address" class="form-control datepicker" />
<span class="input-group-btn"><input type="submit" name="submit_button" value="Request" class="btn btn-default" /></span>
</div>
</form>

<div class="btn-group">
<a href="/dashboard" class="btn btn-default">Back</a>
</div>

<div id="help">
<p>In RAFIS it is possible to act as an information security advisor for another organization. On this page you can submit a request to access the risk analyzes of that other organization.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<img src="/images/icons/advisor.png" class="title_icon" />
<h1>Advisor role</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
