<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs threats">
<thead class="table-xs">
<tr>
<th>Threat</th>
<th>Approach</th>
<th>Urgency</th>
<th>Controls</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="threats/threat">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{/output/content/case/@id}/{@id}'">
<td><span class="table-xs">Threat</span><xsl:value-of select="threat" /></td>
<td><span class="table-xs">Approach</span><xsl:value-of select="handle" /></td>
<td><span class="table-xs">Urgency</span><span class="urgency{risk_value}"><xsl:value-of select="risk_label" /></span></td>
<td><span class="table-xs">Controls</span><xsl:value-of select="controls" /><xsl:if test="(controls=0 and handle!='accept') or (controls>0 and handle='accept')"><img src="/images/warning.png" class="warning" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<xsl:if test="count(threats/threat)>0">
<div class="btn-group right">
<a href="/case/scenarios/{../case/@id}" class="btn btn-default">Continue to scenarios</a>
</div>
</xsl:if>

<div id="help">
<p>An exclamation mark icon may appear in the Controls column. This happens if no controls have yet been selected for an unaccepted threat. It also happens if you accept a threat, but still select controls for it.</p>
<p>Controls for accepted threats are not included in the report and in the progress module!</p>
<p>For threats that are created on the basis of a template, controls have been selected by RAFIS. See this selection as a suggestion by RAFIS. You are advised to carefully evaluate this selection to see if it fits your situation.</p>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<h2><xsl:value-of select="threat/threat" /></h2>
<div class="row">
<div class="col-sm-4"><label>Chance:</label> <xsl:value-of select="threat/chance" /></div>
<div class="col-sm-4"><label>Impact:</label> <xsl:value-of select="threat/impact" /></div>
<div class="col-sm-4"><label>Chosen approach:</label> <xsl:value-of select="threat/handle" /></div>
</div>
<label>Current situation / measures:</label>
<p class="control"><xsl:value-of select="threat/current" /></p>
<label>Desired situation / steps to take:</label>
<p class="control"><xsl:value-of select="threat/action" /></p>

<form action="/{/output/page}/{../case/@id}" method="post">
<input type="hidden" name="case_threat_id" value="{threat/@id}" />
<h2><xsl:value-of select="controls/@standard" /><span class="show_highlighter" onClick="javascript:$('div.highlighter').toggle()">+</span></h2>
<div class="row highlighter">
<div class="col-md-3 col-sm-4 col-xs-12">Highlight controls for:</div>
<div class="col-md-9 col-sm-8 col-xs-12"><select class="form-control threats" onChange="javascript:highlight(this)">
<xsl:for-each select="threats/threat">
<option value="{@id}"><xsl:value-of select="." /></option>
</xsl:for-each>
</select></div>
</div>
<table class="table table-condensed table-striped controls">
<thead>
<tr>
<th>#</th>
<th>Control</th>
<th>Reduces</th>
<th></th>
</tr>
</thead>
<tbody>
<xsl:for-each select="controls/control">
<tr class="control_{@id}"><xsl:if test="effective='no'"><xsl:attribute name="class">ineffective</xsl:attribute></xsl:if>
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="name" /></td>
<td><xsl:value-of select="reduce" /></td>
<td><input type="checkbox" name="selected[]" value="{@id}"><xsl:if test="selected='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save" class="btn btn-default" />
<a href="/{/output/page}/{../case/@id}" class="btn btn-default">Cancel</a>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Controls</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
