<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<form action="/{/output/page}/{../case/@id}" method="post">
<div class="row">
<div class="col-sm-6">
<div><input type="checkbox" name="extra_info" />Add extra explanation.</div>
<div><input type="checkbox" name="accepted_risks" />Include accepted threats.</div>
<div><input type="checkbox" name="sort_by_risk" checked="checked" />Sort by risk / urgency.</div>
</div>
<div class="col-sm-6 backup alert alert-warning">
Don't forget to secure your risk analyses by making an export via <a href="/data">data management</a> and keeping it safe!
</div>
</div>

<div class="btn-group left">
<input type="submit" name="submit_button" value="Generate report" class="btn btn-default" />
</div>
</form>

<div class="btn-group right">
<a href="/case/progress/{../case/@id}" class="btn btn-default">Continue to progress</a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Report</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
