<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />
<div class="interests"><xsl:value-of select="." /></div>

<div class="btn-group left">
<a href="/{/output/page}/{../case/@id}/edit" class="btn btn-default">Edit</a>
<a href="/{/output/page}/{../case/@id}/handout" class="btn btn-default">Generate handout</a>
</div>
<xsl:if test=".!=''">
<div class="btn-group right">
<a href="/case/threats/{../case/@id}" class="btn btn-default">Continue to threats</a>
</div>
</xsl:if>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<form action="/{/output/page}/{../case/@id}" method="post">
<textarea name="interests" class="form-control"><xsl:value-of select="." /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="Save" class="btn btn-default" />
<xsl:if test=".!=''">
<a href="/{/output/page}/{../case/@id}" class="btn btn-default">Cancel</a>
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Interests</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />

<div id="help">
<p>Specify the interests to be protected. This concerns everything that can be affected if the process, to which the information within the scope is supportive, is disrupted. Think, for example, of organizational goals to be achieved and organisation's image, but also of the interests of third parties.</p>
</div>
</xsl:template>

</xsl:stylesheet>
