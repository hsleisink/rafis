<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAFIS license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs threats">
<thead class="table-xs">
<tr>
<th>Threat</th>
<th>Chance</th>
<th>Impact</th>
<th>Approach</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="threats/threat">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{/output/content/case/@id}/{@id}'">
<td><span class="table-xs">Threat</span><xsl:value-of select="threat" /></td>
<td><span class="table-xs">Chance</span><xsl:value-of select="chance" /></td>
<td><span class="table-xs">Impact</span><xsl:value-of select="impact" /></td>
<td><span class="table-xs">Approach</span><xsl:value-of select="handle" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/{/output/content/case/@id}/template" class="btn btn-default">New threat from template</a>
<a href="/{/output/page}/{/output/content/case/@id}/new" class="btn btn-default">New threat</a>
</div>

<xsl:if test="count(threats/threat)>0">
<div class="btn-group right">
<a href="/case/controls/{../case/@id}" class="btn btn-default">Continue to controls</a>
</div>
</xsl:if>

<div id="help">
<p>In this section, you specify the threats that affect the systems within the scope. You can use the RAFIS threat templates or add a threat yourself. In addition to a description, the threat templates contain a selection of controls from the standard chosen for this risk analysis. You can adjust this selection in the next step.</p>
</div>
</xsl:template>

<!--
//
//  Template template
//
//-->
<xsl:template match="templates">
<xsl:call-template name="show_messages" />
<p>Choose one of the generic threats below as the template for the new threat.</p>

<table class="table table-condensed table-hover table-xs templates">
<thead>
<tr>
<th class="number">#</th>
<th class="threat">Threat template</th>
<th class="cia">C</th>
<th class="cia">I</th>
<th class="cia">A</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="template">
<xsl:if test="category">
<tr class="category">
<td colspan="2"><xsl:value-of select="category" /></td>
<td>C</td>
<td>I</td>
<td>A</td>
</tr>
</xsl:if>
<tr onClick="javascript:document.location='/{/output/page}/{../../case/@id}/new/{@id}'">
<td><xsl:value-of select="number" /></td>
<td><div class="threat"><xsl:value-of select="threat" /></div>
<div class="description"><xsl:value-of select="description" /></div></td>
<td><xsl:value-of select="confidentiality" /></td>
<td><xsl:value-of select="integrity" /></td>
<td><xsl:value-of select="availability" /></td>
</tr>
</xsl:for-each>

</tbody>
</table>

<div class="btn-group">
<a href="/{/output/page}/{../case/@id}/new" class="btn btn-default">Don't use a template</a>
<a href="/{/output/page}/{/output/content/case/@id}" class="btn btn-default">Cancel</a>
</div>

<div id="help">
<p>The column C, I and A can contain a p or an s. The p stands for primary and the s for secondary. This indicates whether a threat poses a primary or secondary threat to confidentiality, integrity or availability.</p>
</div>
</xsl:template>

<!--
//
//  Controls template
//
//-->
<xsl:template match="controls">
<table class="table table-condensed">
<thead>
<tr><th>#</th><th>Control from <xsl:value-of select="standard" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="item">
<tr>
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="name" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}/{/output/content/case/@id}/{threat/template_id}" method="post">
<xsl:if test="threat/@id">
<input type="hidden" name="id" value="{threat/@id}" />
</xsl:if>
<xsl:if test="threat/template_id">
<input type="hidden" name="template_id" value="{threat/template_id}" />
</xsl:if>

<xsl:if test="threat/template">
<h2><xsl:value-of select="threat/template" /></h2>
<div class="description"><xsl:value-of select="threat/description" /></div>
<div class="row form-group">
<div class="col-sm-4">Confidentiality: <xsl:value-of select="threat/confidentiality" /></div>
<div class="col-sm-4">Integrity: <xsl:value-of select="threat/integrity" /></div>
<div class="col-sm-4">Availability: <xsl:value-of select="threat/availability" /></div>
</div>
</xsl:if>
<label for="threat">Threat:</label>
<input type="text" id="theat" name="threat" value="{threat/threat}" class="form-control" />
<label for="actor">Most threatening actor:</label>
<select id="actor" name="actor_id" class="form-control">
<xsl:for-each select="actors/actor">
<option value="{@id}"><xsl:if test="@id=../../threat/actor_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<div class="row">
<div class="col-sm-4">
<label for="chance">Chance:</label>
<select id="chance" name="chance" class="form-control">
<xsl:for-each select="chance/option">
<option value="{@value}"><xsl:if test="@value=../../threat/chance"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="col-sm-4">
<label for="impact">Impact:</label>
<select id="impact" name="impact" class="form-control">
<xsl:for-each select="impact/option">
<option value="{@value}"><xsl:if test="@value=../../threat/impact"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="col-sm-4">
<label for="handle">Approach:</label>
<select id="handle" name="handle" class="form-control">
<xsl:for-each select="handle/option">
<option value="{@value}"><xsl:if test="@value=../../threat/handle"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</div>
<label for="argumentation">Substantiation for choices made:</label>
<textarea id="argumentation" name="argumentation" class="form-control"><xsl:value-of select="threat/argumentation" /></textarea>

<h2>Affected systems</h2>
<table class="table table-condensed table-striped table-xs scope">
<thead class="table-xs">
<tr>
<th></th>
<th>Information system</th>
<th>Confidentiality</th>
<th>Integrity</th>
<th>Availability</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="scope/system">
<tr>
<td><span class="table-xs">Affected</span><input type="checkbox" name="threat_scope[]" value="{@id}"><xsl:if test="selected='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></td>
<td><span class="table-xs">Information system</span><xsl:value-of select="item" /></td>
<td><span class="table-xs">Confidentiality</span><xsl:value-of select="confidentiality" /></td>
<td><span class="table-xs">Integrity</span><xsl:value-of select="integrity" /></td>
<td><span class="table-xs">Availability</span><xsl:value-of select="availability" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2>Measures</h2>
<div class="row">
<div class="col-sm-6">
<label for="curent">Current situation / measures:</label>
<textarea id="current" name="current" class="form-control"><xsl:value-of select="threat/current" /></textarea>
</div>
<div class="col-sm-6">
<label for="action">Desired situation / steps to take:</label>
<textarea id="action" name="action" class="form-control"><xsl:value-of select="threat/action" /></textarea>
</div>
</div>
<xsl:apply-templates select="threat/controls" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Save threat" class="btn btn-default" />
<a href="/{/output/page}/{/output/content/case/@id}" class="btn btn-default">Cancel</a>
<xsl:if test="threat/@id">
<input type="submit" name="submit_button" value="Delete threat" class="btn btn-default" onClick="javascript:return confirm('DELETE: Are you sure?')" />
</xsl:if>
</div>
<div class="btn-group">
<a href="/case/risk" target="_blank" class="btn btn-default">Risk assessment</a>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Threats</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="templates" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
